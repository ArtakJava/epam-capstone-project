package com.example.monopolyGame.controller;

import com.example.monopolyGame.dto.LobbyCreateDto;
import com.example.monopolyGame.dto.LobbyDto;
import com.example.monopolyGame.dto.RequestToConnectGameDto;
import com.example.monopolyGame.service.lobbyService.LobbyService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class LobbyController {
    private final LobbyService lobbyService;

    @MessageMapping("/lobbies/getLobbies")
    @SendTo("/lobbies/public")
    public List<LobbyDto> getLobbies() {
        List<LobbyDto> lobbyDtoList = lobbyService.findAllActiveAndNotFulledLobbies();
        return lobbyDtoList;
    }

    @MessageMapping("/lobbies/getLobbies/{lobbyId}")
    @SendToUser("/lobbies/public")
    public LobbyDto getLobbyById(@DestinationVariable long lobbyId) {
        LobbyDto lobbyDto = lobbyService.getLobbyById(lobbyId);
        return lobbyDto;
    }

    @MessageMapping("/lobbies/createLobby")
    @SendTo("/lobbies/public")
    public List<LobbyDto> createLobby(@Payload LobbyCreateDto lobbyCreateDto, Authentication authentication) {
        LobbyDto lobbyDto = lobbyService.createLobby(lobbyCreateDto, authentication);
        List<LobbyDto> lobbyDtoList = lobbyService.findAllActiveAndNotFulledLobbies();
        return lobbyDtoList;
    }

    @MessageMapping("/lobbies/connectToLobby")
    @SendTo("/lobbies/public")
    public List<LobbyDto> connectToLobby(@Payload RequestToConnectGameDto request, Authentication authentication) {
        LobbyDto lobbyDto = lobbyService.connectToLobby(request, authentication);
        List<LobbyDto> lobbyDtoList = lobbyService.findAllActiveAndNotFulledLobbies();
        return lobbyDtoList;
    }
}