package com.example.monopolyGame.controller;

import com.example.monopolyGame.dto.GameDto;
import com.example.monopolyGame.dto.LobbyCreateDto;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.service.gameService.GameService;
import com.example.monopolyGame.service.userService.UserMapper;
import com.example.monopolyGame.service.userService.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/games")
public class GameController {
    private final UserService userService;
    private final GameService gameService;

    @GetMapping
    public String games(Model model, Authentication authentication) {
        LobbyCreateDto lobbyCreateDto = new LobbyCreateDto();
        if (authentication != null) {
            model.addAttribute("game", lobbyCreateDto);
            User currentUser = userService.findByName(authentication.getName());
            model.addAttribute("currentUser", UserMapper.mapToUserDto(currentUser));
        }
        return "games";
    }

    @GetMapping("/{gameId}")
    public String getById(@PathVariable long gameId, Model model, Authentication authentication) {
        if (authentication != null) {
            GameDto gameDto = gameService.getById(gameId, authentication);
            model.addAttribute("game", gameDto);
            User currentUser = userService.findByName(authentication.getName());
            model.addAttribute("currentUser", UserMapper.mapToUserDto(currentUser));
        }
        return "game";
    }

    @GetMapping("/reconnect")
    public String reconnect(Model model, Authentication authentication) {
        if (authentication != null) {
            GameDto gameDto = gameService.reconnect(authentication);
            model.addAttribute("game", gameDto);
            User currentUser = userService.findByName(authentication.getName());
            model.addAttribute("currentUser", UserMapper.mapToUserDto(currentUser));
        }
        return "game";
    }
}