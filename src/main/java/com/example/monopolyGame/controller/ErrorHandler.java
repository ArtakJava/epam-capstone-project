package com.example.monopolyGame.controller;

import com.example.monopolyGame.exception.ApiError;
import com.example.monopolyGame.messageManager.ErrorMessageManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;

@RestControllerAdvice
@Slf4j
public class ErrorHandler {

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ApiError handleUnauthorized(final EntityNotFoundException e) {
        log.info("401 {}", e.getMessage());
        return new ApiError(
                HttpStatus.UNAUTHORIZED,
                ErrorMessageManager.AUTHORIZED_ERROR,
                e.getMessage()
        );
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleThrowable(final Throwable e) {
        log.info("500 {}", e.getMessage());
        return new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                ErrorMessageManager.INTERNAL_SERVER_ERROR,
                e.getMessage()
        );
    }
}