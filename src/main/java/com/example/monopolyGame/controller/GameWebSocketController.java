package com.example.monopolyGame.controller;

import com.example.monopolyGame.dto.GameDto;
import com.example.monopolyGame.service.gameService.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class GameWebSocketController {
    private final SimpMessagingTemplate messagingTemplate;
    private final GameService gameService;

    @MessageMapping("/games/public/{gameId}/leave")
    public String leave(@DestinationVariable long gameId, Authentication authentication) {
        if (authentication != null) {
            GameDto gameDto = gameService.leave(gameId, authentication);
            messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
            messagingTemplate.convertAndSend("/chats/public/" + gameDto.getChat().getId(), gameDto.getMessage());
        }
        return "redirect:/games";
    }

    @MessageMapping("/games/public/{gameId}/rollDice")
    public void rollDice(@DestinationVariable long gameId, Authentication authentication) {
        GameDto gameDto = gameService.rollDice(gameId, authentication);
        messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        messagingTemplate.convertAndSend("/chats/public/" + gameDto.getChat().getId(), gameDto.getMessage());
    }

    @MessageMapping("/games/public/{gameId}/buyField")
    public void buyField(@DestinationVariable long gameId, Authentication authentication) {
        GameDto gameDto = gameService.buyField(gameId, authentication);
        if (gameDto != null) {
            messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        }
    }

    @MessageMapping("/games/public/{gameId}/rejectField")
    public void rejectField(@DestinationVariable long gameId, Authentication authentication) {
        GameDto gameDto = gameService.rejectField(gameId, authentication);
        if (gameDto != null) {
            messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        }
    }

    @MessageMapping("/games/public/{gameId}/pay")
    public void pay(@DestinationVariable long gameId, Authentication authentication) {
        GameDto gameDto = gameService.pay(gameId, authentication);
        if (gameDto != null) {
            messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        }
    }

    @MessageMapping("/games/public/{gameId}/levelUp/{fieldPosition}")
    public void levelUp(@DestinationVariable long gameId, @DestinationVariable long fieldPosition, Authentication authentication) {
        GameDto gameDto = gameService.levelUp(gameId, fieldPosition, authentication);
        messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        messagingTemplate.convertAndSend("/chats/public/" + gameDto.getChat().getId(), gameDto.getMessage());
    }

    @MessageMapping("/games/public/{gameId}/setInBail/{fieldPosition}")
    public void setInBail(@DestinationVariable long gameId, @DestinationVariable long fieldPosition, Authentication authentication) {
        GameDto gameDto = gameService.setInBail(gameId, fieldPosition, authentication);
        messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        messagingTemplate.convertAndSend("/chats/public/" + gameDto.getChat().getId(), gameDto.getMessage());
    }

    @MessageMapping("/games/public/{gameId}/setInRedemption/{fieldPosition}")
    public void setInRedemption(@DestinationVariable long gameId, @DestinationVariable long fieldPosition, Authentication authentication) {
        GameDto gameDto = gameService.setInRedemption(gameId, fieldPosition, authentication);
        messagingTemplate.convertAndSend("/games/public/" + gameDto.getId(), gameDto);
        messagingTemplate.convertAndSend("/chats/public/" + gameDto.getChat().getId(), gameDto.getMessage());
    }
}