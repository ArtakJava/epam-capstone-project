package com.example.monopolyGame.controller;

import com.example.monopolyGame.dto.UserCreateDto;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.service.userService.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/registration")
public class RegistrationController {
    private final UserService userService;

    @GetMapping
    public String showRegistrationForm(Model model) {
        UserCreateDto user = new UserCreateDto();
        model.addAttribute("user", user);
        return "registration";
    }

    @PostMapping("/save")
    public String createUser(@Valid @ModelAttribute("user") UserCreateDto user, BindingResult result, Model model) {
        try {
            User existing = userService.findByName(user.getName());
        } catch (UsernameNotFoundException e) {
            if (result.hasErrors()) {
                model.addAttribute("user", user);
                return "registration";
            }
            userService.createUser(user);
            return "redirect:/registration?success";
        }
        return "registration";
    }
}