package com.example.monopolyGame.controller;

import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.service.userService.UserMapper;
import com.example.monopolyGame.service.userService.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class MainController {
    private static final long PUBLIC_CHAT_ID = 0;
    private final UserService userService;

    @GetMapping
    public String home(Model model, Authentication authentication) {
        if (authentication != null) {
            User currentUser = userService.findByName(authentication.getName());
            model.addAttribute("currentUser", UserMapper.mapToUserDto(currentUser));
        }
        model.addAttribute("chatId", PUBLIC_CHAT_ID);
        return "index";
    }
}