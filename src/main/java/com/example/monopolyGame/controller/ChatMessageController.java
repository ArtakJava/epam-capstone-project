package com.example.monopolyGame.controller;

import com.example.monopolyGame.dto.ChatMessageDto;
import com.example.monopolyGame.service.chatMessageService.ChatMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ChatMessageController {
    private final SimpMessagingTemplate messagingTemplate;
    private final ChatMessageService chatMessageService;

    @MessageMapping("/chats/{chatId}/sendMessage")
    public void sendMessage(@Payload ChatMessageDto chatMessage, Authentication authentication) {
        ChatMessageDto chatMessageDto = chatMessageService.addMessage(chatMessage, authentication);
        messagingTemplate.convertAndSend("/chats/public/" + chatMessageDto.getChatId(), chatMessageDto);
    }

    @MessageMapping("/chats/{chatId}/playerLeftTheGame")
    public void playerLeftTheGame(@Payload ChatMessageDto chatMessage) {
        ChatMessageDto chatMessageDto = chatMessageService.playerLeftTheGame(chatMessage);
        messagingTemplate.convertAndSend("/chats/public/" + chatMessageDto.getChatId(), chatMessageDto);
    }

    @MessageMapping("/chats/{chatId}/getMessages")
    public void getMessages(@DestinationVariable Long chatId) {
        List<ChatMessageDto> messages = chatMessageService.getMessagesByChat(chatId);
        messagingTemplate.convertAndSend("/chats/public/" + chatId + "/getMessages", messages);
    }
}