package com.example.monopolyGame.exception;

public class ChanceNotFoundException extends RuntimeException {
    public ChanceNotFoundException(String message) {
        super(message);
    }
}