package com.example.monopolyGame.exception;

public class FieldActionException extends RuntimeException {
    public FieldActionException(String message) {
        super(message);
    }
}