package com.example.monopolyGame.exception;

public class PlayerCanNotPayException extends RuntimeException {
    public PlayerCanNotPayException(String message) {
        super(message);
    }
}