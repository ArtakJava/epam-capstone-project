package com.example.monopolyGame.exception;

public class UserAlreadyPlayException extends RuntimeException {
    public UserAlreadyPlayException(String message) {
        super(message);
    }
}