package com.example.monopolyGame.exception;

public class FieldNotAcquirableException extends RuntimeException {
    public FieldNotAcquirableException(String message) {
        super(message);
    }
}