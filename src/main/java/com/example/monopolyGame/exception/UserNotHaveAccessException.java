package com.example.monopolyGame.exception;

public class UserNotHaveAccessException extends RuntimeException {
    public UserNotHaveAccessException(String message) {
        super(message);
    }
}