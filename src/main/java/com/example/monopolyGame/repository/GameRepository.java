package com.example.monopolyGame.repository;

import com.example.monopolyGame.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

    @Query("select g " +
            "from Game as g " +
            "where :userId in (select user.id from Player as p join p.user user where p member of g.players) " +
            "and g.state != 'FINISHED'")
    Game findActiveGameByUserId(long userId);
}