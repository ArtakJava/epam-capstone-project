package com.example.monopolyGame.repository;

import com.example.monopolyGame.entity.Lobby;
import com.example.monopolyGame.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LobbyRepository extends JpaRepository<Lobby, Long> {

    @Query("select l " +
            "from Lobby as l " +
            "where l.users.size < l.numberOfPlayers " +
            "and l.active = 'true'")
    List<Lobby> findAllActiveAndNotFulledLobbies();

    @Query("select l " +
            "from Lobby as l " +
            "where :user member of l.users " +
            "and l.active = 'true'")
    List<Lobby> findActiveLobbiesByUser(User user);

    @Query("select l " +
            "from Lobby as l " +
            "where :user member of l.users and l.id != :currentLobbyId " +
            "and l.active = 'true'")
    List<Lobby> findOtherActiveLobbiesByUser(User user, long currentLobbyId);
}