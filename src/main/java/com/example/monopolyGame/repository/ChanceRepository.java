package com.example.monopolyGame.repository;

import com.example.monopolyGame.entity.field.Chance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChanceRepository extends JpaRepository<Chance, Long> {
}