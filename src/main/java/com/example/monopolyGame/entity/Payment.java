package com.example.monopolyGame.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "payments", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "amount")
    private int amount;
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private PaymentType type;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "payer_id")
    private Player payer;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "payee_id")
    private Player payee;
}