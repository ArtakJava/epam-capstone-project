package com.example.monopolyGame.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "lobbies", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lobby {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Builder.Default
    @Column(name = "active")
    private boolean active = true;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "lobbies_users",
            joinColumns = @JoinColumn(name = "lobby_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;
    @Column(name = "number_of_players")
    private int numberOfPlayers;
}