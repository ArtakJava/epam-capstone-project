package com.example.monopolyGame.entity;

import com.example.monopolyGame.entity.field.TableField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "games", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Game {
    @Transient
    private static final int DEFAULT_TURN_TIME = 60;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Builder.Default
    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private GameState state = GameState.ROLL;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "payment_id")
    private Payment payment;
    @OneToOne
    @JoinColumn(name = "chat_id")
    private Chat chat;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "current_player_id")
    private Player currentPlayer;
    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(
            name = "games_players",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "player_id"))
    private List<Player> players;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "games_fields",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "field_id"))
    private List<TableField> fields;
    @Builder.Default
    @Column(name = "end_time_for_turn")
    private Instant endTimeForTurn = Instant.now().plusSeconds(DEFAULT_TURN_TIME);


    public Optional<Player> findPlayer(User currentUser) {
        return players.stream()
                .filter(player -> player.getUser().getId() == currentUser.getId())
                .findAny();
    }

    public List<TableField> getFields() {
        Collections.sort(fields);
        return fields;
    }

    public List<Player> getPlayers() {
        Collections.sort(players);
        return players;
    }

    public void updateEndTimeForTurn() {
        endTimeForTurn = Instant.now().plusSeconds(DEFAULT_TURN_TIME);
    }
}