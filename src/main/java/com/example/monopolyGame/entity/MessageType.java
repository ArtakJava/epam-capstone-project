package com.example.monopolyGame.entity;

public enum MessageType {
    CHAT, EVENT, LEFT
}