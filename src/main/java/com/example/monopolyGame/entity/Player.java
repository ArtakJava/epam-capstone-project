package com.example.monopolyGame.entity;

import com.example.monopolyGame.entity.field.TableField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "players", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Player implements Comparable<Player> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "last_dice_double_number")
    private boolean lastDiceDoubleNumber;
    @Column(name = "mute")
    private boolean mute;
    @Builder.Default
    @Column(name = "active")
    private boolean active = true;
    @Column(name = "double_dice_number_in_order")
    private int doubleDiceNumberInOrder;
    @Column(name = "turn_number")
    private int turnNumber;
    @Column(name = "in_jail")
    private boolean inJail;
    @Builder.Default
    @Column(name = "balance")
    private int balance = 15000;
    @Column(name = "roll_count_in_jail")
    private int rollCountInJail;
    @Column(name = "color")
    private String color;
    @Builder.Default
    @Column(name = "position")
    private int position = 1;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "players_fields",
            joinColumns = @JoinColumn(name = "player_id"),
            inverseJoinColumns = @JoinColumn(name = "field_id"))
    private List<TableField> fields;

    @Override
    public int compareTo(Player o) {
        return this.getTurnNumber() - o.getTurnNumber();
    }
}