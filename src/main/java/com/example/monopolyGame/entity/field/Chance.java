package com.example.monopolyGame.entity.field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "chances", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Chance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "description")
    private String description;
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private ChanceType type;
    @Column(name = "pay")
    private Integer pay;
}