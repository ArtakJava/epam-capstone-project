package com.example.monopolyGame.entity.field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.lang.reflect.Field;

@Entity
@Inheritance
@Table(name = "fields", schema = "public")
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class TableField implements Comparable<TableField> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;
    @Column(name = "name")
    protected String name;
    @Column(name = "position")
    protected int position;
    @Column(name = "img_src")
    protected String imgSrc;

    public TableField(String name, int position, String imgSrc) {
        this.name = name;
        this.position = position;
        this.imgSrc = imgSrc;
    }

    @Override
    public int compareTo(TableField o) {
        return this.getPosition() - o.getPosition();
    }

    private  <T> boolean isFieldExists(Class<T> tClass, String fieldName) {
        if (tClass == null) {
            return false;
        }
        Field[] fields = tClass.getDeclaredFields();
        boolean result = false;
        for (Field field : fields) {
            if (field.getName().equals(fieldName)) {
                result = true;
                break;
            }
        }
        return result || isFieldExists(tClass.getSuperclass(), fieldName);
    }

    public boolean isFieldExists(String fieldName) {
        return isFieldExists(this.getClass(), fieldName);
    }
}