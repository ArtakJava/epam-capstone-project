package com.example.monopolyGame.entity.field;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.PreUpdate;
import java.util.List;

@Entity
@NoArgsConstructor
public class HiringCompanyField extends LevelDependencyField {

    public HiringCompanyField(String name, int position, String imgSrc, FieldGroup group, int price) {
        super(name, position, imgSrc, group, price, 250);
    }

    @PreUpdate
    public void updateLevel() {
        if (getOwner() != null) {
            List<TableField> fields = getOwner().getFields();
            level = (int) fields.stream()
                    .filter(field -> field instanceof HiringCompanyField)
                    .count() - 1;
        } else {
            level = 0;
        }
    }

    public int getLevel() {
        updateLevel();
        return level;
    }

    @Override
    public int getFirstLevelRent() {
        return baseRent * (int) Math.pow(2, 1);
    }

    @Override
    public int getSecondLevelRent() {
        return baseRent * (int) Math.pow(2, 2);
    }

    @Override
    public int getThirdLevelRent() {
        return baseRent * (int) Math.pow(2, 3);
    }
}