package com.example.monopolyGame.entity.field;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class EmptyField extends TableField {

    public EmptyField(int position, String imgSrc) {
        super("Empty Field", position, imgSrc);
    }
}