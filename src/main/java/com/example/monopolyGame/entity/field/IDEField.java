package com.example.monopolyGame.entity.field;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class IDEField extends AcquirableField implements FactorDependencyRentable {

    public IDEField(String name, int position, String imgSrc, FieldGroup group, int price, int baseRent) {
        super(name, position, imgSrc, group, price, baseRent);
    }

    @Override
    public int getFactor() {
        int factor = 0;
        if (getOwner() != null) {
            long playerFieldCount = getOwner().getFields().stream()
                    .filter(field -> field instanceof IDEField)
                    .count();
            if (playerFieldCount == 1) {
                factor = getFirstLevelFactor();
            } else if (playerFieldCount == 2) {
                factor = getSecondLevelFactor();
            }
        }
        return factor;
    }

    @Override
    public int getFirstLevelFactor() {
        return 100;
    }

    @Override
    public int getSecondLevelFactor() {
        return 250;
    }
}