package com.example.monopolyGame.entity.field;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class GoToDeployField extends TableField {

    public GoToDeployField(int position, String imgSrc) {
        super("Go to deploy", position, imgSrc);
    }
}