package com.example.monopolyGame.entity.field;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
public abstract class LevelDependencyField extends AcquirableField  {
    @Column(name = "level")
    protected int level;

    public LevelDependencyField(String name, int position, String imgSrc, FieldGroup group, int price, int baseRent) {
        super(name, position, imgSrc, group, price, baseRent);
    }

    public int getRent() {
        switch (getLevel()) {
            case 0:
                return baseRent;
            case 1:
                return getFirstLevelRent();
            case 2:
                return getSecondLevelRent();
            case 3:
                return getThirdLevelRent();
        }
        return baseRent;
    }

    public abstract int getFirstLevelRent();

    public abstract int getSecondLevelRent();

    public abstract int getThirdLevelRent();
}