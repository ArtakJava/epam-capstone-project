package com.example.monopolyGame.entity.field;

public interface FactorDependencyRentable {

    int getFactor();

    int getFirstLevelFactor();

    int getSecondLevelFactor();
}