package com.example.monopolyGame.entity.field;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class UpgradeableField extends LevelDependencyField {
    @Column(name = "upgradeable")
    private boolean upgradeable;
    @Column(name = "max_level")
    private int maxLevel;

    public UpgradeableField(String name, int position, String imgSrc, FieldGroup group, int price, int baseRent) {
        super(name, position, imgSrc, group, price, baseRent);
    }

    @Override
    public int getFirstLevelRent() {
        return baseRent * 5;
    }

    @Override
    public int getSecondLevelRent() {
        return baseRent * 15;
    }

    @Override
    public int getThirdLevelRent() {
        return baseRent * 45;
    }
}