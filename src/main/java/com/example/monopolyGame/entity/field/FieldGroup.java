package com.example.monopolyGame.entity.field;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "field_groups", schema = "public")
@Data
@NoArgsConstructor
public class FieldGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "level_up_cost")
    private int levelUpCost;
    @Column(name = "number_of_fields")
    private int numberOfFields;
    @Column(name = "price_color")
    private String priceColor;
    @Column(name = "rent_color")
    private String rentColor;

    public FieldGroup(String name, int levelUpCost, int numberOfFields, String priceColor, String rentColor) {
        this.name = name;
        this.levelUpCost = levelUpCost;
        this.numberOfFields = numberOfFields;
        this.priceColor = priceColor;
        this.rentColor = rentColor;
    }
}