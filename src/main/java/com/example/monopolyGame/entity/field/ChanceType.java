package com.example.monopolyGame.entity.field;

public enum ChanceType {
    PAYABLE, MUTE_NEXT_TURN, WINNABLE
}