package com.example.monopolyGame.entity.field;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class ChanceField extends TableField {

    public ChanceField(int position, String imgSrc) {
        super("Chance", position, imgSrc);
    }
}