package com.example.monopolyGame.entity.field;

import com.example.monopolyGame.entity.Player;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance
@Data
@NoArgsConstructor
public abstract class AcquirableField extends TableField {
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "group_id")
    protected FieldGroup group;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "player_id")
    protected Player owner;
    @Column(name = "price")
    protected int price;
    @Column(name = "bail")
    protected int bail;
    @Column(name = "in_bail")
    protected boolean inBail;
    @Column(name = "redemption")
    protected int redemption;
    @Column(name = "base_rent")
    protected int baseRent;

    public AcquirableField(String name, int position, String imgSrc, FieldGroup group, int price, int baseRent) {
        super(name, position, imgSrc);
        this.group = group;
        this.price = price;
        this.bail = (int) (price * 0.5);
        this.redemption = (int) (price * 0.6);
        this.baseRent = baseRent;
    }
}