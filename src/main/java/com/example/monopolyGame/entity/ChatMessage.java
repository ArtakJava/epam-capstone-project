package com.example.monopolyGame.entity;

import lombok.*;

import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "messages", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne()
    @JoinColumn(name = "chat_id")
    private Chat chat;
    @Column(name = "text")
    private String text;
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User sender;
    @Column(name = "type")
    private MessageType type;
}