package com.example.monopolyGame.entity;

public enum PaymentType {
    FIELD, RENT, CHANCE
}