package com.example.monopolyGame.utils;

import com.example.monopolyGame.entity.field.*;

import java.util.ArrayList;
import java.util.List;

public class FieldGeneratorUtils {

    private FieldGeneratorUtils() {
    }

    public static List<TableField> getFields() {
        List<TableField> fields = new ArrayList<>();
        EmptyField startField = new EmptyField(1, "/images/start.png");
        EmptyField holiday = new EmptyField(21, "/images/holiday.png");
        EmptyField holiday1 = new EmptyField(5, "/images/holiday.png");
        EmptyField holiday2 = new EmptyField(37, "/images/holiday.png");
        EmptyField deploymentField = new EmptyField(11, "/images/deployment.png");
        GoToDeployField goToDeployField = new GoToDeployField(31, "/images/go-to-deploy.png");
        ChanceField chanceFieldThreePosition = new ChanceField(3, "/images/chance.png");
        ChanceField chanceFieldEightPosition = new ChanceField(8, "/images/chance.png");
        ChanceField chanceFieldEighteenPosition = new ChanceField(18, "/images/chance.png");
        ChanceField chanceFieldTwentyThreePosition = new ChanceField(23, "/images/chance.png");
        ChanceField chanceFieldThirtyFourPosition = new ChanceField(34, "/images/chance.png");
        ChanceField chanceFieldThirtyNinePosition = new ChanceField(39, "/images/chance.png");


        FieldGroup containerization = new FieldGroup("Containerization", 500, 2, "#8d6a18", "#b17c00");
        UpgradeableField docker = new UpgradeableField("Docker", 2, "/images/docker.png", containerization, 600, 20);
        UpgradeableField kubernetes = new UpgradeableField("Kubernetes", 4, "/images/kubernetes.png", containerization, 600, 40);

        FieldGroup dataBase = new FieldGroup("Data-base", 500, 3, "#185b8d", "#0075cd");
        UpgradeableField mysql = new UpgradeableField("mySQL", 7, "/images/my-sql.png", dataBase, 1000, 60);
        UpgradeableField postgresql = new UpgradeableField("PostgreSQL", 9, "/images/postgresql.png", dataBase, 1000, 60);
        UpgradeableField mongodb = new UpgradeableField("MongoDB", 10, "/images/mongo-db.png", dataBase, 1200, 80);

        FieldGroup language = new FieldGroup("Language", 750, 3, "#678d18", "#7da922");
        UpgradeableField js = new UpgradeableField("JS", 12, "/images/js.png", language, 1400, 100);
        UpgradeableField python = new UpgradeableField("Python", 14, "/images/python.png", language, 1400, 100);
        UpgradeableField java = new UpgradeableField("Java", 15, "/images/java.png", language, 1600, 120);

        FieldGroup versionControl = new FieldGroup("Version control system", 1000, 3, "#8d1818", "#dd1a1a");
        UpgradeableField bitBucket = new UpgradeableField("BitBucket", 17, "/images/bitbucket.png", versionControl, 1800, 140);
        UpgradeableField gitLab = new UpgradeableField("GitLab", 19, "/images/gitlab.png", versionControl, 1800, 140);
        UpgradeableField gitHub = new UpgradeableField("GitHub", 20, "/images/github.png", versionControl, 2000, 160);

        FieldGroup messageBroker = new FieldGroup("Message broker", 1250, 3, "#398d18", "#36c100");
        UpgradeableField activemq = new UpgradeableField("ActiveMQ", 22, "/images/activemq.png", messageBroker, 2200, 180);
        UpgradeableField rabbitmq = new UpgradeableField("RabbitMQ", 24, "/images/rabbitmq.png", messageBroker, 2200, 180);
        UpgradeableField kafka = new UpgradeableField("Kafka", 25, "/images/kafka.png", messageBroker, 2400, 200);

        FieldGroup cloudService = new FieldGroup("Cloud service", 1500, 3, "#8d8a18", "#bfba00");
        UpgradeableField googleCloud = new UpgradeableField("Google Cloud", 27, "/images/google-cloud.png", cloudService, 2600, 220);
        UpgradeableField azure = new UpgradeableField("Azure", 28, "/images/azure.png", cloudService, 2600, 220);
        UpgradeableField aws = new UpgradeableField("AWS", 30, "/images/aws.png", cloudService, 2800, 240);

        FieldGroup itService = new FieldGroup("It service", 1750, 3, "#188d7e", "#00b19a");
        UpgradeableField luxoft = new UpgradeableField("Luxoft", 32, "/images/luxoft.png", itService, 3000, 260);
        UpgradeableField accenture = new UpgradeableField("Accenture", 33, "/images/accenture.png", itService, 3000, 260);
        UpgradeableField epam = new UpgradeableField("EPAM", 35, "/images/epam.png", itService, 3200, 280);

        FieldGroup itEcoSystem = new FieldGroup("It ecosystem", 2000, 2, "#af1e9c", "#e500c7");
        UpgradeableField google = new UpgradeableField("Google", 38, "/images/google.png", itEcoSystem, 3500, 350);
        UpgradeableField apple = new UpgradeableField("Apple", 40, "/images/apple.png", itEcoSystem, 4000, 500);

        FieldGroup hiringCompany = new FieldGroup("Hiring company", 0, 4, "#181b8d", "#1d21db");
        HiringCompanyField dice = new HiringCompanyField("Dice", 6, "/images/dice.png", hiringCompany, 2000);
        HiringCompanyField indeed = new HiringCompanyField("Indeed", 16, "/images/indeed.png", hiringCompany, 2000);
        HiringCompanyField glassDoor = new HiringCompanyField("GlassDoor", 26, "/images/glassdoor.png", hiringCompany, 2000);
        HiringCompanyField linkedin = new HiringCompanyField("Linkedin", 36, "/images/linkedIn.png", hiringCompany, 2000);

        FieldGroup ideCompany = new FieldGroup("IDE", 0, 2, "#66188d", "#8300c5");
        IDEField eclipse = new IDEField("Eclipse", 13, "/images/eclipse.png", ideCompany, 1500, 0);
        IDEField intellij = new IDEField("Intellij IDEA", 29, "/images/intellij-idea.png", ideCompany, 1500, 0);

        fields.add(docker);
        fields.add(kubernetes);
        fields.add(mysql);
        fields.add(postgresql);
        fields.add(mongodb);
        fields.add(js);
        fields.add(python);
        fields.add(java);
        fields.add(bitBucket);
        fields.add(gitLab);
        fields.add(gitHub);
        fields.add(activemq);
        fields.add(rabbitmq);
        fields.add(kafka);
        fields.add(googleCloud);
        fields.add(azure);
        fields.add(aws);
        fields.add(luxoft);
        fields.add(accenture);
        fields.add(epam);
        fields.add(google);
        fields.add(apple);
        fields.add(dice);
        fields.add(indeed);
        fields.add(glassDoor);
        fields.add(linkedin);
        fields.add(eclipse);
        fields.add(intellij);
        fields.add(startField);
        fields.add(holiday);
        fields.add(holiday1);
        fields.add(holiday2);
        fields.add(deploymentField);
        fields.add(goToDeployField);
        fields.add(chanceFieldThreePosition);
        fields.add(chanceFieldEightPosition);
        fields.add(chanceFieldEighteenPosition);
        fields.add(chanceFieldTwentyThreePosition);
        fields.add(chanceFieldThirtyFourPosition);
        fields.add(chanceFieldThirtyNinePosition);
        return fields;
    }
}