package com.example.monopolyGame.utils;

import com.example.monopolyGame.dto.DiceResultDto;

public final class DiceUtils {
    private static final int DICE_SIZE = 6;

    private DiceUtils() {
    }

    public static DiceResultDto roll() {
        int firstNumber = getDiceNumber();
        int secondNumber = getDiceNumber();
        return new DiceResultDto(firstNumber, secondNumber);
    }

    private static int getDiceNumber() {
        return (int) (Math.random() * DICE_SIZE) + 1;
    }
}