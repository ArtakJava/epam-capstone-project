package com.example.monopolyGame.dto;

import com.example.monopolyGame.messageManager.ErrorMessageManager;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDto {
    private long id;
    @NotBlank(message = ErrorMessageManager.USER_NAME_EMPTY)
    @Size(min = 3, max = 10, message = ErrorMessageManager.USER_NAME_MIN_3_MAX_10)
    private String name;
    @NotBlank(message = ErrorMessageManager.USER_PASSWORD_EMPTY)
    @Size(min = 2, max = 10, message = ErrorMessageManager.USER_PASSWORD_MIN_2_MAX_10)
    private String password;
    @NotBlank(message = ErrorMessageManager.USER_EMAIL_EMPTY)
    @Email(message = ErrorMessageManager.USER_EMAIL)
    @Size(min = 6, max = 254, message = ErrorMessageManager.USER_EMAIL_MIN_6_MAX_254)
    private String email;
}