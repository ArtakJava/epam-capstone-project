package com.example.monopolyGame.dto;

import com.example.monopolyGame.messageManager.ErrorMessageManager;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LobbyCreateDto {
    private String creatorName;
    @Min(value = 2, message = ErrorMessageManager.NUMBER_OF_PLAYERS_MIN_2_MAX_5)
    @Max(value = 5, message = ErrorMessageManager.NUMBER_OF_PLAYERS_MIN_2_MAX_5)
    private int numberOfPlayers;
}