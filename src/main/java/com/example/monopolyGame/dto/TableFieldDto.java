package com.example.monopolyGame.dto;

import com.example.monopolyGame.entity.field.FieldGroup;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TableFieldDto {
    protected String name;
    protected int position;
    protected String imgSrc;
    protected FieldGroup group;
    protected PlayerDto owner;
    protected Integer price;
    protected long bail;
    protected long redemption;
    protected int baseRent;
    private int firstLevelRent;
    private int secondLevelRent;
    private int thirdLevelRent;
    protected Integer currentRent;
    protected Integer factor;
    protected Integer firstLevelFactor;
    protected Integer secondLevelFactor;
    private int level;
    private boolean upgradeable;
    private boolean inBail;
}