package com.example.monopolyGame.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessageDto {
    private long chatId;
    private UserDto sender;
    private String text;
    private String type;
}