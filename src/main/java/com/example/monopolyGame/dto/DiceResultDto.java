package com.example.monopolyGame.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DiceResultDto {
    private int firstNumber;
    private int secondNumber;

    public int getResult() {
        return firstNumber + secondNumber;
    }
}