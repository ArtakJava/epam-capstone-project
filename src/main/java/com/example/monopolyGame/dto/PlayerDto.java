package com.example.monopolyGame.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDto {
    private int position;
    private boolean active;
    private boolean inJail;
    private UserDto user;
    private int balance;
    private String color;
}