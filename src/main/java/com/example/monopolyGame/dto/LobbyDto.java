package com.example.monopolyGame.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LobbyDto {
    private long id;
    private Set<UserDto> players;
    private int numberOfPlayers;
    private boolean fulled;
}