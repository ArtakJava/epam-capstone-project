package com.example.monopolyGame.dto;

import com.example.monopolyGame.entity.GameState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameDto {
    private long id;
    private boolean nextTurn;
    private GameState state;
    private ChatMessageDto message;
    private ChatDto chat;
    private PlayerDto currentPlayer;
    private List<PlayerDto> players;
    private List<TableFieldDto> fields;
    private Instant endTimeForTurn;
    private DiceResultDto diceResult;
    private List<TableFieldDto> updatedFields;
    private PaymentDto payment;
}