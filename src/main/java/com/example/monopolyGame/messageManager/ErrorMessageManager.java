package com.example.monopolyGame.messageManager;

public class ErrorMessageManager {
    public static final String USER_NAME_EMPTY = "User name can not be empty.";
    public static final String USER_NAME_MIN_3_MAX_10 = "User name must be in the range of 3 - 10 characters.";
    public static final String USER_PASSWORD_MIN_2_MAX_10 = "The user password must be in the range of 2 - 10 characters.";
    public static final String USER_EMAIL_EMPTY = "User email cannot be empty.";
    public static final String USER_EMAIL = "Incorrect email.";
    public static final String USER_PASSWORD_EMPTY = "User password cannot be empty.";
    public static final String USER_EMAIL_MIN_6_MAX_254 = "Email must be in the range of 6 - 254 characters.";
    public static final String USER_NAME_NOT_FOUND = "User name %s not found.";
    public static final String GAME_NAME_NOT_FOUND = "Game with ID - %s not found.";
    public static final String AUTHORIZED_ERROR = "Incorrect login or password.";
    public static final String INTERNAL_SERVER_ERROR = "Internal server error.";
    public static final String NUMBER_OF_PLAYERS_MIN_2_MAX_5 = "The number of players should be in the range of 2 - 5 players.";
    public static final String LOBBY_NOT_FOUND = "Lobby with ID - %s not found.";
    public static final String CHAT_NOT_FOUND = "Chat with ID - %s not found.";
    public static final String FIELD_NOT_FOUND = "Field not found.";
    public static final String USER_ALREADY_PLAY = "User %s already play game.";
    public static final String USER_NOT_HAVE_ACCESS = "User %s not have access.";
    public static final String FIELD_NOT_ACQUIRABLE = "Field %s not Acquirable.";
    public static final String USER_NOT_HAVE_ENOUGH_BALANCE = "User %s not have enough balance.";
    public static final String CHANCE_NOT_FOUND = "Chance %s not found.";
    public static final String FIELD_ACTION_NOT_DONE = "Field action can not done.";
}