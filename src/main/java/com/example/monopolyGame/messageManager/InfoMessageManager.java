package com.example.monopolyGame.messageManager;

public class InfoMessageManager {
    public static final String ROLL_RESULT = "%s roll %s and %s. ";
    public static final String GO_TO_DEPLOY_BECAUSE_THREE_DOUBLE_ROLL =
            "And go to deploy because he rolled double number three times in order. ";
    public static final String FREE_FROM_DEPLOY = "Roll double number and free from deploy. ";
    public static final String THINK_ABOUT_BUY = "Think about buy field. ";
    public static final String NEED_TO_PAY_RENT = "And need to pay %s for rent to %s.";
    public static final String GO_TO_DEPLOY_BECAUSE_GO_TO_DEPLOY_FIELD = "And go to deploy because he is in deploy field. ";
    public static final String ROLL_DOUBLE_NUMBER = "Roll double number and rolling again. ";
    public static final String GO_TO_CHANCE = "Go to chance and ";
    public static final String GO_TO_OWN_FIELD = "Go to own field. ";
    public static final String GO_TO_FREE_FIELD = "Go to free field. ";
    public static final String PLAYER_LEFT_THE_GAME_DUE_INACTIVITY = "User %s left the game due to inactivity. ";
    public static final String GET_ACTIVE_NOT_FULLED_LOBBIES = "Request for find all active not fulled lobbies. ";
    public static final String CREATE_LOBBY = "Creating lobby - %s. ";
    public static final String CONNECT_TO_LOBBY = "User - %s connect to lobby - %s. ";
    public static final String ALREADY_CONNECTED_TO_LOBBY = "User - %s already connected to lobby with ID - %s. ";
    public static final String ADD_MESSAGE = "Add message - %s. ";
    public static final String GET_MESSAGES_BY_CHAT = "Get all messages by chat with id - %s. ";
    public static final String CREATE_CHAT = "Created chat with id - %s. ";
    public static final String CREATE_PLAYERS = "created players for game. ";
    public static final String CREATE_USER = "Created user with name - %s. ";
    public static final String SET_ACTIVE_FOR_PLAYERS = "Set active for players. ";
    public static final String GET_GAME = "Get game with id - %s. ";
    public static final String CREATE_GAME = "Created game - %s. ";
    public static final String BUY_FIELD = "User %s buy field. ";
    public static final String REJECT_FIELD = "User %s reject field. ";
    public static final String LEVEL_UP_FIELD = "User %s level up field. ";
    public static final String GAME_FINISHED = "Game finished. ";
    public static final String SET_IN_JAIL = "User %s set field in jail . ";
    public static final String OUT_FROM_JAIL = "User %s get out field from jail. ";
    public static final String LEFT_GAME = "User %s left the game. ";
    public static final String PAY = "User %s pay. ";
}