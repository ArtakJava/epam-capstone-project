package com.example.monopolyGame.service.gameService;

import com.example.monopolyGame.dto.GameDto;
import com.example.monopolyGame.entity.User;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface GameService {

    GameDto createGame(List<User> users);

    GameDto getById(long gameId, Authentication authentication);

    GameDto rollDice(long gameId, Authentication authentication);

    GameDto buyField(long gameId, Authentication authentication);

    GameDto rejectField(long gameId, Authentication authentication);

    GameDto pay(long gameId, Authentication authentication);

    GameDto levelUp(long gameId, long fieldPosition, Authentication authentication);

    GameDto setInBail(long gameId, long fieldPosition, Authentication authentication);

    GameDto setInRedemption(long gameId, long fieldPosition, Authentication authentication);

    GameDto reconnect(Authentication authentication);

    GameDto leave(long gameId, Authentication authentication);
}