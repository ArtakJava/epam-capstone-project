package com.example.monopolyGame.service.gameService;

import com.example.monopolyGame.dto.ChatMessageDto;
import com.example.monopolyGame.dto.DiceResultDto;
import com.example.monopolyGame.dto.GameDto;
import com.example.monopolyGame.entity.Chat;
import com.example.monopolyGame.entity.Game;
import com.example.monopolyGame.entity.Player;
import com.example.monopolyGame.entity.field.TableField;
import com.example.monopolyGame.service.chatService.ChatMapper;
import com.example.monopolyGame.service.playerService.PlayerMapper;
import com.example.monopolyGame.utils.FieldGeneratorUtils;

import java.util.List;
import java.util.stream.Collectors;

public class GameMapper {
    public static Game mapToNewGame(List<Player> players) {
        return Game.builder()
                .chat(new Chat())
                .currentPlayer(players.stream().sorted().findFirst().get())
                .players(players)
                .fields(FieldGeneratorUtils.getFields())
                .build();
    }

    public static GameDto mapToGameDto(Game game) {
        return GameDto.builder()
                .id(game.getId())
                .state(game.getState())
                .payment(game.getPayment() != null ? PaymentMapper.mapToPaymentDto(game.getPayment()) : null)
                .chat(ChatMapper.mapToChatDto(game.getChat()))
                .currentPlayer(PlayerMapper.mapToPlayerDto(game.getCurrentPlayer()))
                .players(game.getPlayers().stream()
                        .map(PlayerMapper::mapToPlayerDto)
                        .collect(Collectors.toList()))
                .fields(game.getFields().stream()
                        .map(FieldMapper::mapToFieldDto)
                        .collect(Collectors.toList()))
                .endTimeForTurn(game.getEndTimeForTurn())
                .build();
    }

    public static GameDto mapToGameDtoWithMessage(Game game, ChatMessageDto message) {
        GameDto gameDto = mapToGameDto(game);
        gameDto.setMessage(message);
        return gameDto;
    }

    public static GameDto mapToGameDtoWithDiceResult(Game game, DiceResultDto diceRollResult, ChatMessageDto message) {
        GameDto gameDto = mapToGameDtoWithMessage(game, message);
        gameDto.setDiceResult(diceRollResult);
        return gameDto;
    }

    public static GameDto mapToGameDtoWithUpdatedFields(Game game, List<TableField> fieldsForUpdate) {
        GameDto gameDto = mapToGameDto(game);
        gameDto.setUpdatedFields(fieldsForUpdate.stream()
                .map(FieldMapper::mapToFieldDto)
                .collect(Collectors.toList()));
        return gameDto;
    }
}