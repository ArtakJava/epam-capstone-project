package com.example.monopolyGame.service.gameService;

import com.example.monopolyGame.dto.PaymentDto;
import com.example.monopolyGame.entity.Payment;
import com.example.monopolyGame.entity.PaymentType;
import com.example.monopolyGame.entity.Player;
import com.example.monopolyGame.service.playerService.PlayerMapper;

public class PaymentMapper {

    public static Payment createPayment(int payAmount, Player currentPlayer, Player owner, PaymentType type) {
        return Payment.builder()
                .amount(payAmount)
                .type(type)
                .payer(currentPlayer)
                .payee(owner)
                .build();
    }

    public static PaymentDto mapToPaymentDto(Payment payment) {
        return PaymentDto.builder()
                .amount(payment.getAmount())
                .payee(payment.getPayee() != null ? PlayerMapper.mapToPlayerDto(payment.getPayee()) : null)
                .payer(PlayerMapper.mapToPlayerDto(payment.getPayer()))
                .build();
    }
}