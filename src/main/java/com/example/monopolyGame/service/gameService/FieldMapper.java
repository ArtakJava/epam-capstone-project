package com.example.monopolyGame.service.gameService;

import com.example.monopolyGame.dto.TableFieldDto;
import com.example.monopolyGame.entity.Player;
import com.example.monopolyGame.entity.field.*;
import com.example.monopolyGame.service.playerService.PlayerMapper;

public class FieldMapper {
    public static TableFieldDto mapToFieldDto(TableField tableField) {
        TableFieldDto result = TableFieldDto.builder()
                .name(tableField.getName())
                .position(tableField.getPosition())
                .imgSrc(tableField.getImgSrc())
                .build();
        if (tableField instanceof AcquirableField) {
            result.setGroup(((AcquirableField) tableField).getGroup());
            Player owner = ((AcquirableField) tableField).getOwner();
            if (owner != null) {
                result.setOwner(PlayerMapper.mapToPlayerDto(owner));
            }
            result.setPrice(((AcquirableField) tableField).getPrice());
            result.setBail(((AcquirableField) tableField).getBail());
            result.setInBail(((AcquirableField) tableField).isInBail());
            result.setRedemption(((AcquirableField) tableField).getRedemption());
            result.setBaseRent(((AcquirableField) tableField).getBaseRent());
        }
        if (tableField instanceof FactorDependencyRentable) {
            result.setFactor(((FactorDependencyRentable) tableField).getFactor());
            result.setFirstLevelFactor(((FactorDependencyRentable) tableField).getFirstLevelFactor());
            result.setSecondLevelFactor(((FactorDependencyRentable) tableField).getSecondLevelFactor());
        }
        if (tableField instanceof LevelDependencyField) {
            result.setCurrentRent(((LevelDependencyField) tableField).getRent());
            result.setFirstLevelRent(((LevelDependencyField) tableField).getFirstLevelRent());
            result.setSecondLevelRent(((LevelDependencyField) tableField).getSecondLevelRent());
            result.setThirdLevelRent(((LevelDependencyField) tableField).getThirdLevelRent());
        }
        if (tableField instanceof UpgradeableField) {
            result.setLevel(((UpgradeableField) tableField).getLevel());
            result.setUpgradeable(((UpgradeableField) tableField).isUpgradeable());
        }
        return result;
    }
}