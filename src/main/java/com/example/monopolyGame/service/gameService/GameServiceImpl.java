package com.example.monopolyGame.service.gameService;

import com.example.monopolyGame.dto.ChatMessageDto;
import com.example.monopolyGame.dto.DiceResultDto;
import com.example.monopolyGame.dto.GameDto;
import com.example.monopolyGame.entity.*;
import com.example.monopolyGame.entity.field.*;
import com.example.monopolyGame.exception.*;
import com.example.monopolyGame.messageManager.ErrorMessageManager;
import com.example.monopolyGame.messageManager.InfoMessageManager;
import com.example.monopolyGame.repository.ChanceRepository;
import com.example.monopolyGame.repository.GameRepository;
import com.example.monopolyGame.service.chatMessageService.ChatMessageMapper;
import com.example.monopolyGame.service.chatMessageService.ChatMessageService;
import com.example.monopolyGame.service.chatService.ChatService;
import com.example.monopolyGame.service.playerService.PlayerService;
import com.example.monopolyGame.service.userService.UserService;
import com.example.monopolyGame.utils.DiceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class GameServiceImpl implements GameService {
    private final TransactionTemplate transactionTemplate;
    private final SimpMessagingTemplate messagingTemplate;
    private final Map<Long, ScheduledFuture<?>> timers = new HashMap<>();
    private static final int DEPLOYMENT_POSITION = 11;
    private static final int ROLL_COUNT_WHEN_PLAYER_GO_TO_FROM_JAIL = 3;
    private static final int BONUS_FOR_FULL_CIRCLE = 2000;
    private final UserService userService;
    private final ChatService chatService;
    private final ChatMessageService chatMessageService;
    private final PlayerService playerService;
    private final GameRepository gameRepository;
    private final ChanceRepository chanceRepository;

    @Override
    public GameDto createGame(List<User> users) {
        List<Player> playersForGame = getPlayersForGame(users);
        Game game = GameMapper.mapToNewGame(playersForGame);
        Chat chat = chatService.createChat();
        userService.setActiveUsers(users);
        game.setChat(chat);
        GameDto gameDto = GameMapper.mapToGameDto(gameRepository.save(game));
        startTimerForTurn(game);
        log.info(String.format(InfoMessageManager.CREATE_GAME, gameDto));
        return gameDto;
    }

    @Override
    public GameDto getById(long gameId, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        if (game.findPlayer(user).isPresent()) {
            log.info(String.format(InfoMessageManager.GET_GAME, game.getId()));
            return GameMapper.mapToGameDto(game);
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    private void leaveCurrentPlayerFromGame(Game game) {
        game.setState(GameState.ROLL);
        Player currentPlayer = game.getCurrentPlayer();
        disablePlayer(currentPlayer);
        setNewCurrentPlayer(game);
    }

    private void startTimerForTurn(Game game) {
        long gameId = game.getId();
        if (timers.containsKey(gameId)) {
            timers.get(gameId).cancel(false);
            timers.remove((gameId));
        }
        ScheduledExecutorService timerService = Executors.newScheduledThreadPool(1);
        long time = ChronoUnit.MILLIS.between(Instant.now(), game.getEndTimeForTurn());
        ScheduledFuture<?> timer = timerService.schedule(new TimerTask() {

            @Override
            public void run() {
                transactionTemplate.execute(status -> {
                    Game gameForNextTurn = findGameById(gameId);
                    Player currentPlayer = gameForNextTurn.getCurrentPlayer();
                    leaveCurrentPlayerFromGame(gameForNextTurn);
                    StringBuilder messageBuilder = new StringBuilder();
                    messageBuilder.append(String.format(InfoMessageManager.PLAYER_LEFT_THE_GAME_DUE_INACTIVITY, currentPlayer.getUser().getName()));
                    if (GameState.FINISHED.equals(gameForNextTurn.getState())) {
                        messageBuilder.append(InfoMessageManager.GAME_FINISHED);
                    }
                    ChatMessageDto chatMessageDto = ChatMessageMapper.createChatMessageDto(
                            gameForNextTurn.getChat().getId(),
                            currentPlayer.getUser(),
                            messageBuilder.toString(),
                            MessageType.LEFT
                    );
                    chatMessageService.playerLeftTheGame(chatMessageDto);
                    GameDto gameDto = GameMapper.mapToGameDtoWithMessage(gameRepository.save(gameForNextTurn), chatMessageDto);
                    gameDto.setNextTurn(true);
                    messagingTemplate.convertAndSend("/games/public/" + gameId, gameDto);
                    messagingTemplate.convertAndSend("/chats/public/" + gameDto.getChat().getId(), gameDto.getMessage());
                    if (!GameState.FINISHED.equals(gameDto.getState())) {
                        startTimerForTurn(gameForNextTurn);
                    }
                    return null;
                });
            }
        }, time, TimeUnit.MILLISECONDS);
        timers.put(gameId, timer);
    }

    @Transactional
    @Override
    public GameDto rollDice(long gameId, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        Player currentPlayer = game.getCurrentPlayer();
        if (user.getId() == currentPlayer.getUser().getId() && GameState.ROLL.equals(game.getState())) {
            StringBuilder messageBuilder = new StringBuilder();
            DiceResultDto diceRollResult = DiceUtils.roll();
            messageBuilder.append(String.format(
                    InfoMessageManager.ROLL_RESULT, currentPlayer.getUser().getName(),
                    diceRollResult.getFirstNumber(),
                    diceRollResult.getSecondNumber()
            ));
            if (diceRollResult.getFirstNumber() == diceRollResult.getSecondNumber()) {
                if (currentPlayer.isInJail()) {
                    messageBuilder.append(InfoMessageManager.FREE_FROM_DEPLOY);
                    currentPlayer.setInJail(false);
                }
                int currentPlayerDoubleDiceNumberInOrder = currentPlayer.getDoubleDiceNumberInOrder();
                if (currentPlayerDoubleDiceNumberInOrder == 2) {
                    movePlayerToDeployment(currentPlayer);
                    setNextTurn(game);
                    messageBuilder.append(InfoMessageManager.GO_TO_DEPLOY_BECAUSE_THREE_DOUBLE_ROLL);
                    ChatMessageDto chatMessageDto = ChatMessageMapper.createChatMessageDto(
                            game.getChat().getId(),
                            currentPlayer.getUser(),
                            messageBuilder.toString(),
                            MessageType.EVENT
                    );
                    chatMessageService.addMessage(chatMessageDto, authentication);
                    return GameMapper.mapToGameDtoWithDiceResult(gameRepository.save(game), diceRollResult, chatMessageDto);
                } else {
                    currentPlayer.setLastDiceDoubleNumber(true);
                    currentPlayer.setDoubleDiceNumberInOrder(++currentPlayerDoubleDiceNumberInOrder);
                }
                messageBuilder.append(InfoMessageManager.ROLL_DOUBLE_NUMBER);
            } else {
                currentPlayer.setDoubleDiceNumberInOrder(0);
            }
            if (!currentPlayer.isInJail()) {
                int diceResult = diceRollResult.getResult();
                setPlayerPosition(game, diceResult);
                Optional<TableField> optionalField = game.getFields().stream()
                        .filter(tableField -> tableField.getPosition() == currentPlayer.getPosition())
                        .findAny();
                if (optionalField.isPresent()) {
                    TableField field = optionalField.get();
                    if (field instanceof AcquirableField) {
                        Player owner = ((AcquirableField) field).getOwner();
                        if (owner == null) {
                            int fieldPrice = ((AcquirableField) field).getPrice();
                            checkHaveEnoughBalance(currentPlayer, fieldPrice);
                            game.setState(GameState.BUY_ANSWER);
                            messageBuilder.append(InfoMessageManager.THINK_ABOUT_BUY);
                            Payment payment = PaymentMapper.createPayment
                                    (fieldPrice, currentPlayer, null, PaymentType.FIELD);
                            game.setPayment(payment);
                        } else {
                            if (!((AcquirableField) field).isInBail()) {
                                if (owner.getUser().getId() != currentPlayer.getUser().getId()) {
                                    int payAmount = 0;
                                    if (field instanceof LevelDependencyField) {
                                        payAmount = ((LevelDependencyField) field).getRent();
                                    } else if (field instanceof FactorDependencyRentable) {
                                        payAmount = ((FactorDependencyRentable) field).getFactor() * diceRollResult.getResult();
                                    }
                                    Payment payment = PaymentMapper.createPayment
                                            (payAmount, currentPlayer, owner, PaymentType.RENT);
                                    game.setPayment(payment);
                                    messageBuilder.append(String.format(
                                            InfoMessageManager.NEED_TO_PAY_RENT,
                                            payAmount,
                                            owner.getUser().getName()
                                    ));
                                    game.setState(GameState.PAY);
                                } else {
                                    messageBuilder.append(InfoMessageManager.GO_TO_OWN_FIELD);
                                }
                            } else {
                                messageBuilder.append(InfoMessageManager.GO_TO_FREE_FIELD);
                            }
                        }
                    } else if (field instanceof GoToDeployField) {
                        movePlayerToDeployment(currentPlayer);
                        messageBuilder.append(InfoMessageManager.GO_TO_DEPLOY_BECAUSE_GO_TO_DEPLOY_FIELD);
                    } else if (field instanceof ChanceField) {
                        List<Chance> chances = chanceRepository.findAll();
                        int chanceId = (int) (Math.random() * chances.size()) + 1;
                        Optional<Chance> currentChance = chances.stream()
                                .filter(chance -> chance.getId() == chanceId)
                                .findAny();
                        if (currentChance.isPresent()) {
                            ChanceType type = currentChance.get().getType();
                            int currentPlayerBalance = currentPlayer.getBalance();
                            switch (type) {
                                case PAYABLE:
                                    Payment payment = PaymentMapper.createPayment
                                            (currentChance.get().getPay(), currentPlayer, null, PaymentType.CHANCE);
                                    game.setPayment(payment);
                                    messageBuilder.append(InfoMessageManager.GO_TO_CHANCE)
                                            .append(currentChance.get().getDescription())
                                            .append(currentChance.get().getPay());
                                    game.setState(GameState.PAY);
                                    break;
                                case WINNABLE:
                                    currentPlayerBalance += currentChance.get().getPay();
                                    currentPlayer.setBalance(currentPlayerBalance);
                                    messageBuilder.append(InfoMessageManager.GO_TO_CHANCE)
                                            .append(currentChance.get().getDescription())
                                            .append(currentChance.get().getPay());
                                    break;
                                case MUTE_NEXT_TURN:
                                    currentPlayer.setMute(true);
                                    messageBuilder.append(InfoMessageManager.GO_TO_CHANCE)
                                            .append(currentChance.get().getDescription());
                                    break;
                            }
                        } else {
                            throw new ChanceNotFoundException(String.format(ErrorMessageManager.CHANCE_NOT_FOUND, user.getName()));
                        }
                    } else {
                        messageBuilder.append(InfoMessageManager.GO_TO_FREE_FIELD);
                    }
                    if (GameState.ROLL.equals(game.getState())) {
                        setNextTurn(game);
                    }
                    ChatMessageDto chatMessageDto = ChatMessageMapper.createChatMessageDto(
                            game.getChat().getId(),
                            currentPlayer.getUser(),
                            messageBuilder.toString(),
                            MessageType.EVENT
                    );
                    chatMessageService.addMessage(chatMessageDto, authentication);
                    return GameMapper.mapToGameDtoWithDiceResult(gameRepository.save(game), diceRollResult, chatMessageDto);
                } else {
                    throw new FieldNotFoundException(ErrorMessageManager.FIELD_NOT_FOUND);
                }
            } else {
                int rollCountInJail = currentPlayer.getRollCountInJail();
                currentPlayer.setRollCountInJail(++rollCountInJail);
                game.setState(GameState.ROLL);
                setNextTurn(game);
                ChatMessageDto chatMessageDto = ChatMessageMapper.createChatMessageDto(
                        game.getChat().getId(),
                        currentPlayer.getUser(),
                        messageBuilder.toString(),
                        MessageType.EVENT
                );
                chatMessageService.addMessage(chatMessageDto, authentication);
                return GameMapper.mapToGameDtoWithDiceResult(gameRepository.save(game), diceRollResult, chatMessageDto);
            }
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    private void setNextTurn(Game game) {
        setNewCurrentPlayer(game);
        game.updateEndTimeForTurn();
        if (GameState.ROLL.equals(game.getState())) {
            startTimerForTurn(game);
        }
    }

    private Game findGameById(long gameId) {
        return gameRepository.findById(gameId).orElseThrow(
                () -> new GameNotFoundException(String.format(ErrorMessageManager.GAME_NAME_NOT_FOUND, gameId))
        );
    }

    private void setPlayerPosition(Game game, int diceResult) {
        int tableSize = game.getFields().size();
        Player currentPlayer = game.getCurrentPlayer();
        int currentPlayerPosition = currentPlayer.getPosition();
        currentPlayerPosition += diceResult;
        if (currentPlayerPosition > game.getFields().size()) {
            currentPlayerPosition -= tableSize;
            getBonusForFullCircle(currentPlayer);
        }
        currentPlayer.setPosition(currentPlayerPosition);
    }

    private void getBonusForFullCircle(Player currentPlayer) {
        int balance = currentPlayer.getBalance();
        balance += BONUS_FOR_FULL_CIRCLE;
        currentPlayer.setBalance(balance);
    }

    private void movePlayerToDeployment(Player currentPlayer) {
        currentPlayer.setLastDiceDoubleNumber(false);
        currentPlayer.setDoubleDiceNumberInOrder(0);
        currentPlayer.setInJail(true);
        currentPlayer.setPosition(DEPLOYMENT_POSITION);
    }

    @Transactional
    @Override
    public GameDto buyField(long gameId, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        Player currentPlayer = game.getCurrentPlayer();
        if (user.getId() == currentPlayer.getUser().getId() && GameState.BUY_ANSWER.equals(game.getState())) {
            int currentPlayerPosition = currentPlayer.getPosition();
            TableField field = getFieldByPosition(game, currentPlayerPosition);
            if (field instanceof AcquirableField) {
                int fieldPrice = ((AcquirableField) field).getPrice();
                checkHaveEnoughBalance(currentPlayer, fieldPrice);
                int currentPlayerBalance = currentPlayer.getBalance();
                game.setState(GameState.ROLL);
                currentPlayerBalance -= fieldPrice;
                currentPlayer.setBalance(currentPlayerBalance);
                ((AcquirableField) field).setOwner(currentPlayer);
                currentPlayer.getFields().add(field);
                setNextTurn(game);
                List <TableField> fieldsForUpdate = new ArrayList<>();
                if (field instanceof HiringCompanyField) {
                    fieldsForUpdate = currentPlayer.getFields().stream()
                            .filter(currentField -> currentField instanceof HiringCompanyField)
                            .collect(Collectors.toList());
                } else if (field instanceof FactorDependencyRentable) {
                    fieldsForUpdate = currentPlayer.getFields().stream()
                            .filter(currentField -> currentField instanceof FactorDependencyRentable)
                            .collect(Collectors.toList());
                } else if (field instanceof UpgradeableField){
                    setUpgradeAfterBoughtFieldIfIsFullSet((UpgradeableField) field);
                    fieldsForUpdate.add(field);
                }
                GameDto gameDto = GameMapper.mapToGameDtoWithUpdatedFields(game, fieldsForUpdate);
                log.info(String.format(InfoMessageManager.BUY_FIELD, user.getName()));
                return gameDto;
            } else {
                throw new FieldNotAcquirableException(String.format(ErrorMessageManager.FIELD_NOT_ACQUIRABLE, user.getName()));
            }
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    private void setUpgradeAfterBoughtFieldIfIsFullSet(UpgradeableField field) {
        List<UpgradeableField> playerUpgradeableFieldsByGroupId = getUpgradeableFullSetFields(field);
        int groupNumberOfFields = field.getGroup().getNumberOfFields();
        if (groupNumberOfFields == playerUpgradeableFieldsByGroupId.size()) {
            boolean haveBailedFields = playerUpgradeableFieldsByGroupId.stream()
                    .anyMatch(AcquirableField::isInBail);
            if (haveBailedFields) {
                playerUpgradeableFieldsByGroupId.forEach(tableField -> tableField.setUpgradeable(false));
            } else {
                Optional<Integer> minimalLevel = playerUpgradeableFieldsByGroupId.stream()
                        .map(UpgradeableField::getLevel)
                        .sorted()
                        .findFirst();
                if (minimalLevel.isPresent()) {
                    playerUpgradeableFieldsByGroupId.stream()
                            .filter(tableField -> tableField.getLevel() == minimalLevel.get())
                            .forEach(tableField -> tableField.setUpgradeable(true));
                    playerUpgradeableFieldsByGroupId.stream()
                            .filter(tableField -> tableField.getLevel() != minimalLevel.get() ||
                                    tableField.getMaxLevel() == minimalLevel.get())
                            .forEach(tableField -> tableField.setUpgradeable(false));
                }
            }
        } else {
            playerUpgradeableFieldsByGroupId.forEach(tableField -> tableField.setUpgradeable(false));
        }
    }

    private List<UpgradeableField> getUpgradeableFullSetFields(UpgradeableField field) {
        Player owner = field.getOwner();
        return owner.getFields().stream()
                .filter(tableField -> tableField instanceof UpgradeableField)
                .map(tableField -> ((UpgradeableField) tableField))
                .filter(tableField -> tableField.getGroup().getId() == field.getGroup().getId())
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public GameDto rejectField(long gameId, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        Player currentPlayer = game.getCurrentPlayer();
        if (user.getId() == currentPlayer.getUser().getId() && GameState.BUY_ANSWER.equals(game.getState())) {
            game.setState(GameState.ROLL);
            setNextTurn(game);
            GameDto gameDto = GameMapper.mapToGameDto(game);
            gameDto.setNextTurn(true);
            log.info(String.format(InfoMessageManager.REJECT_FIELD, user.getName()));
            return gameDto;
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    @Transactional
    @Override
    public GameDto pay(long gameId, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        Player currentPlayer = game.getCurrentPlayer();
        Payment payment = game.getPayment();
        if (user.getId() == currentPlayer.getUser().getId() && GameState.PAY.equals(game.getState()) &&
                payment != null) {
            Player payer = payment.getPayer();
            if (user.getId() == payer.getUser().getId()) {
                int paymentAmount = payment.getAmount();
                checkHaveEnoughBalance(payer, paymentAmount);
                int payerBalance = payer.getBalance();
                payerBalance -= paymentAmount;
                payer.setBalance(payerBalance);
                Player owner = payment.getPayee();
                if (owner != null) {
                    int ownerBalance = owner.getBalance();
                    ownerBalance += paymentAmount;
                    owner.setBalance(ownerBalance);
                }
                game.setState(GameState.ROLL);
                setNextTurn(game);
                log.info(String.format(InfoMessageManager.PAY, user.getName()));
                return GameMapper.mapToGameDto(game);
            } else {
                throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
            }
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    @Transactional
    @Override
    public GameDto levelUp(long gameId, long fieldPosition, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        TableField fieldForUpgrade = getFieldByPosition(game, fieldPosition);
        if (fieldForUpgrade instanceof UpgradeableField) {
            if (((UpgradeableField) fieldForUpgrade).isUpgradeable() && !((UpgradeableField) fieldForUpgrade).isInBail()) {
                Player currentPlayer = game.getCurrentPlayer();
                long currentUserId = currentPlayer.getUser().getId();
                if (user.getId() == currentUserId && ((UpgradeableField) fieldForUpgrade).getOwner().getUser().getId() == currentUserId &&
                        GameState.ROLL.equals(game.getState())) {
                    int paymentForUpdate = ((UpgradeableField) fieldForUpgrade).getGroup().getLevelUpCost();
                    checkHaveEnoughBalance(currentPlayer, paymentForUpdate);
                    int level = ((UpgradeableField) fieldForUpgrade).getLevel();
                    ((UpgradeableField) fieldForUpgrade).setLevel(++level);
                    setUpgradeAfterBoughtFieldIfIsFullSet((UpgradeableField) fieldForUpgrade);
                    int balance = ((UpgradeableField) fieldForUpgrade).getOwner().getBalance();
                    balance -= paymentForUpdate;
                    ((UpgradeableField) fieldForUpgrade).getOwner().setBalance(balance);
                    List <TableField> fieldsForUpdate = new ArrayList<>();
                    fieldsForUpdate.add(fieldForUpgrade);
                    GameDto gameDto = GameMapper.mapToGameDtoWithUpdatedFields(game, fieldsForUpdate);
                    log.info(String.format(InfoMessageManager.LEVEL_UP_FIELD, user.getName()));
                    return gameDto;
                } else {
                    throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
                }
            } else {
                throw new FieldActionException(ErrorMessageManager.FIELD_ACTION_NOT_DONE);
            }
        } else {
            throw new FieldActionException(ErrorMessageManager.FIELD_ACTION_NOT_DONE);
        }
    }

    @Transactional
    @Override
    public GameDto setInBail(long gameId, long fieldPosition, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        TableField fieldForSetInJail = getFieldByPosition(game, fieldPosition);
        if (fieldForSetInJail instanceof AcquirableField) {
            if (!((AcquirableField) fieldForSetInJail).isInBail()) {
                Player currentPlayer = game.getCurrentPlayer();
                long currentUserId = currentPlayer.getUser().getId();
                if (user.getId() == currentUserId && ((AcquirableField) fieldForSetInJail).getOwner().getUser().getId() == currentUserId &&
                        GameState.ROLL.equals(game.getState())) {
                    int jailBonus = ((AcquirableField) fieldForSetInJail).getBail();
                    ((AcquirableField) fieldForSetInJail).setInBail(true);
                    int balance = ((AcquirableField) fieldForSetInJail).getOwner().getBalance();
                    balance += jailBonus;
                    ((AcquirableField) fieldForSetInJail).getOwner().setBalance(balance);
                    List <TableField> fieldsForUpdate = new ArrayList<>();
                    fieldsForUpdate.add(fieldForSetInJail);
                    if (fieldForSetInJail instanceof UpgradeableField) {
                        setUpgradeAfterBoughtFieldIfIsFullSet((UpgradeableField) fieldForSetInJail);
                    }
                    GameDto gameDto = GameMapper.mapToGameDtoWithUpdatedFields(game, fieldsForUpdate);
                    log.info(String.format(InfoMessageManager.SET_IN_JAIL, user.getName()));
                    return gameDto;
                } else {
                    throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
                }
            } else {
                throw new FieldActionException(ErrorMessageManager.FIELD_ACTION_NOT_DONE);
            }
        } else {
            throw new FieldActionException(ErrorMessageManager.FIELD_ACTION_NOT_DONE);
        }
    }

    @Transactional
    @Override
    public GameDto setInRedemption(long gameId, long fieldPosition, Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = findGameById(gameId);
        TableField fieldForSetInRedemption = getFieldByPosition(game, fieldPosition);
        if (fieldForSetInRedemption instanceof AcquirableField) {
            if (((AcquirableField) fieldForSetInRedemption).isInBail()) {
                Player currentPlayer = game.getCurrentPlayer();
                long currentUserId = currentPlayer.getUser().getId();
                if (user.getId() == currentUserId && ((AcquirableField) fieldForSetInRedemption).getOwner().getUser().getId() == currentUserId &&
                        GameState.ROLL.equals(game.getState())) {
                    int redemptionPrice = ((AcquirableField) fieldForSetInRedemption).getRedemption();
                    checkHaveEnoughBalance(currentPlayer, redemptionPrice);
                    ((AcquirableField) fieldForSetInRedemption).setInBail(false);
                    int balance = ((AcquirableField) fieldForSetInRedemption).getOwner().getBalance();
                    balance -= redemptionPrice;
                    ((AcquirableField) fieldForSetInRedemption).getOwner().setBalance(balance);
                    List <TableField> fieldsForUpdate = new ArrayList<>();
                    fieldsForUpdate.add(fieldForSetInRedemption);
                    if (fieldForSetInRedemption instanceof UpgradeableField) {
                        setUpgradeAfterBoughtFieldIfIsFullSet((UpgradeableField) fieldForSetInRedemption);
                    }
                    GameDto gameDto = GameMapper.mapToGameDtoWithUpdatedFields(game, fieldsForUpdate);
                    log.info(String.format(InfoMessageManager.OUT_FROM_JAIL, user.getName()));
                    return gameDto;
                } else {
                    throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
                }
            } else {
                throw new FieldActionException(ErrorMessageManager.FIELD_ACTION_NOT_DONE);
            }
        } else {
            throw new FieldActionException(ErrorMessageManager.FIELD_ACTION_NOT_DONE);
        }
    }

    @Override
    public GameDto reconnect(Authentication authentication) {
        User user = userService.findByName(authentication.getName());
        Game game = gameRepository.findActiveGameByUserId(user.getId());
        if (game.findPlayer(user).isPresent()) {
            log.info(String.format(InfoMessageManager.GET_GAME, game.getId()));
            return GameMapper.mapToGameDto(game);
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    @Transactional
    @Override
    public GameDto leave(long gameId, Authentication authentication) {
        Game game = findGameById(gameId);
        User user = userService.findByName(authentication.getName());
        if (game.findPlayer(user).isPresent()) {
            if (game.getCurrentPlayer().getUser().getId() == user.getId()) {
                leaveCurrentPlayerFromGame(game);
            } else {
                disablePlayer(game.findPlayer(user).get());
            }
            checkGameIsFinished(game);
            StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append(String.format(InfoMessageManager.LEFT_GAME, user.getName()));
            if (GameState.FINISHED.equals(game.getState())) {
                messageBuilder.append(InfoMessageManager.GAME_FINISHED);
            }
            ChatMessageDto chatMessageDto = ChatMessageMapper.createChatMessageDto(
                    game.getChat().getId(),
                    user,
                    messageBuilder.toString(),
                    MessageType.LEFT
            );
            GameDto gameDto = GameMapper.mapToGameDtoWithMessage(gameRepository.save(game), chatMessageDto);
            log.info(String.format(InfoMessageManager.LEFT_GAME, user.getName()));
            return gameDto;
        } else {
            throw new UserNotHaveAccessException(String.format(ErrorMessageManager.USER_NOT_HAVE_ACCESS, user.getName()));
        }
    }

    private void disablePlayer(Player player) {
        player.setActive(false);
        player.getUser().setActive(false);
        player.getFields().clear();
    }

    private TableField getFieldByPosition(Game game, long fieldPosition) {
        return game.getFields().stream()
                .filter(field -> field.getPosition() == fieldPosition)
                .findAny().orElseThrow(() -> new FieldNotFoundException(ErrorMessageManager.FIELD_NOT_FOUND));
    }

    private void checkHaveEnoughBalance(Player payer, int paymentAmount) {
        if (payer.getBalance() < paymentAmount) {
            throw new PlayerCanNotPayException(String.format(ErrorMessageManager.USER_NOT_HAVE_ENOUGH_BALANCE, payer.getUser().getName()));
        }
    }

    private void setNewCurrentPlayer(Game game) {
        List<Player> players = game.getPlayers();
        Player currentPlayer = game.getCurrentPlayer();
        if (!currentPlayer.isLastDiceDoubleNumber()) {
            int currentPlayerIndex = players.indexOf(currentPlayer);
            Player nextPlayer;

            do {
                currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                nextPlayer = players.get(currentPlayerIndex);
                if (nextPlayer.isMute()) {
                    nextPlayer.setMute(false);
                    currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
                    nextPlayer = players.get(currentPlayerIndex);
                }
            } while (!nextPlayer.isActive());
            Optional<Player> newCurrentPlayer = Optional.of(nextPlayer);
            checkFreedomFromJail(newCurrentPlayer.get());
            game.setCurrentPlayer(newCurrentPlayer.get());
        } else {
            currentPlayer.setLastDiceDoubleNumber(false);
        }
        game.updateEndTimeForTurn();
        checkGameIsFinished(game);
    }

    private void checkGameIsFinished(Game game) {
        List<Player> players = game.getPlayers();
        long activeCountPlayers = players.stream()
                .filter(Player::isActive)
                .count();
        if (activeCountPlayers < 2) {
            players.forEach(player -> {
                player.setActive(false);
                player.getUser().setActive(false);
            });
            game.setState(GameState.FINISHED);
        }
    }

    private void checkFreedomFromJail(Player newCurrentPlayer) {
        if (newCurrentPlayer.getRollCountInJail() == ROLL_COUNT_WHEN_PLAYER_GO_TO_FROM_JAIL) {
            newCurrentPlayer.setInJail(false);
        }
    }

    private List<Player> getPlayersForGame(List<User> users) {
        return playerService.getPlayersForGame(users);
    }
}