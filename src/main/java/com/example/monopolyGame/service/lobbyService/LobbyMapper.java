package com.example.monopolyGame.service.lobbyService;

import com.example.monopolyGame.dto.LobbyCreateDto;
import com.example.monopolyGame.dto.LobbyDto;
import com.example.monopolyGame.entity.Lobby;
import com.example.monopolyGame.service.userService.UserMapper;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class LobbyMapper {

    public static LobbyDto mapLobbyToLobbyDto(Lobby lobby) {
        return LobbyDto.builder()
                .id(lobby.getId())
                .players(lobby.getUsers().stream()
                        .map(UserMapper::mapToUserDto)
                        .collect(Collectors.toSet()))
                .numberOfPlayers(lobby.getNumberOfPlayers())
                .build();
    }

    public static Lobby mapLobbyCreateDtoToLobby(LobbyCreateDto lobbyCreateDto) {
        return Lobby.builder()
                .users(new ArrayList<>())
                .numberOfPlayers(lobbyCreateDto.getNumberOfPlayers())
                .build();
    }
}