package com.example.monopolyGame.service.lobbyService;

import com.example.monopolyGame.dto.GameDto;
import com.example.monopolyGame.dto.LobbyCreateDto;
import com.example.monopolyGame.dto.LobbyDto;
import com.example.monopolyGame.dto.RequestToConnectGameDto;
import com.example.monopolyGame.entity.Lobby;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.exception.LobbyNotFoundException;
import com.example.monopolyGame.exception.UserAlreadyPlayException;
import com.example.monopolyGame.messageManager.ErrorMessageManager;
import com.example.monopolyGame.messageManager.InfoMessageManager;
import com.example.monopolyGame.repository.LobbyRepository;
import com.example.monopolyGame.service.gameService.GameService;
import com.example.monopolyGame.service.userService.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class LobbyServiceImpl implements LobbyService {
    private final SimpMessagingTemplate messagingTemplate;
    private final UserService userService;
    private final LobbyRepository lobbyRepository;
    private final GameService gameService;

    @Transactional
    @Override
    public List<LobbyDto> findAllActiveAndNotFulledLobbies() {
        List<LobbyDto> lobbyDtoList = lobbyRepository.findAllActiveAndNotFulledLobbies().stream()
                .map(LobbyMapper::mapLobbyToLobbyDto)
                .collect(Collectors.toList());
        log.info(InfoMessageManager.GET_ACTIVE_NOT_FULLED_LOBBIES);
        return lobbyDtoList;
    }

    @Override
    public LobbyDto createLobby(LobbyCreateDto lobbyCreateDto, Authentication authentication) {
        User creator = userService.findByName(authentication.getName());
        if (!creator.isActive()) {
            disconnectFromOtherLobbies(creator, null);
            lobbyCreateDto.setCreatorName(creator.getName());
            Lobby lobby = LobbyMapper.mapLobbyCreateDtoToLobby(lobbyCreateDto);
            lobby.getUsers().add(creator);
            LobbyDto lobbyDto = LobbyMapper.mapLobbyToLobbyDto(lobbyRepository.save(lobby));
            log.info(String.format(InfoMessageManager.CREATE_LOBBY, lobbyDto));
            return lobbyDto;
        } else {
            throw new UserAlreadyPlayException(String.format(ErrorMessageManager.USER_ALREADY_PLAY, creator.getName()));
        }
    }

    @Override
    public LobbyDto connectToLobby(RequestToConnectGameDto request, Authentication authentication) {
        User requestor = userService.findByName(authentication.getName());
        if (!requestor.isActive()) {
            disconnectFromOtherLobbies(requestor, request.getLobbyId());
            Lobby lobby = findById(request.getLobbyId());
            if (isRequestorAlreadyConnected(requestor, lobby)) {
                log.info(String.format(InfoMessageManager.ALREADY_CONNECTED_TO_LOBBY, requestor.getName(), lobby.getId()));
                return LobbyMapper.mapLobbyToLobbyDto(lobby);
            }
            lobby.getUsers().add(requestor);
            if (lobby.getUsers().size() == lobby.getNumberOfPlayers()) {
                lobby.setActive(false);
                GameDto game = gameService.createGame(lobby.getUsers());
                messagingTemplate.convertAndSend("/lobbies/public/" + lobby.getId(), game);
            }
            LobbyDto lobbyDto = LobbyMapper.mapLobbyToLobbyDto(lobbyRepository.save(lobby));
            log.info(String.format(InfoMessageManager.CONNECT_TO_LOBBY, requestor.getName(), lobbyDto));
            return lobbyDto;
        } else {
            throw new UserAlreadyPlayException(String.format(ErrorMessageManager.USER_ALREADY_PLAY, requestor.getName()));
        }
    }

    private boolean isRequestorAlreadyConnected(User requestor, Lobby lobby) {
        return lobby.getUsers().contains(requestor);
    }

    @Override
    public Lobby findById(long id) {
        return lobbyRepository.findById(id).orElseThrow(
                () -> new LobbyNotFoundException(String.format(ErrorMessageManager.LOBBY_NOT_FOUND, id))
        );
    }

    @Override
    public LobbyDto getLobbyById(long id) {
        return LobbyMapper.mapLobbyToLobbyDto(findById(id));
    }

    private void disconnectFromOtherLobbies(User user, Long currentLobbyId) {
        List<Lobby> otherLobbies;
        List<Lobby> emptyLobbies = new ArrayList<>();
        if (currentLobbyId != null) {
            otherLobbies = lobbyRepository.findOtherActiveLobbiesByUser(user, currentLobbyId);
        } else {
            otherLobbies = lobbyRepository.findActiveLobbiesByUser(user);
        }
        otherLobbies.forEach(lobby -> {
            lobby.getUsers().remove(user);
            if (lobby.getUsers().isEmpty()) {
                emptyLobbies.add(lobby);
            }
        });
        lobbyRepository.saveAll(otherLobbies);
        lobbyRepository.deleteAll(emptyLobbies);
    }
}