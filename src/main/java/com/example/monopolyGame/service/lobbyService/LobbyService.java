package com.example.monopolyGame.service.lobbyService;

import com.example.monopolyGame.dto.LobbyCreateDto;
import com.example.monopolyGame.dto.LobbyDto;
import com.example.monopolyGame.dto.RequestToConnectGameDto;
import com.example.monopolyGame.entity.Lobby;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface LobbyService {

    List<LobbyDto> findAllActiveAndNotFulledLobbies();

    LobbyDto createLobby(LobbyCreateDto lobbyCreateDto, Authentication authentication);

    LobbyDto connectToLobby(RequestToConnectGameDto request, Authentication authentication);

    Lobby findById(long id);

    LobbyDto getLobbyById(long id);
}