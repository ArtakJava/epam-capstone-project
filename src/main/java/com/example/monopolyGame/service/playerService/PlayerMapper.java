package com.example.monopolyGame.service.playerService;

import com.example.monopolyGame.dto.PlayerDto;
import com.example.monopolyGame.entity.Player;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.service.userService.UserMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerMapper {
    private static final List<String> PLAYER_COLOR_SET = List.of("#ff9c9c", "#fffd9e", "#a6ff8b", "#8bffda", "#ac96ff");

    public static List<Player> mapToNewPlayer(List<User> users) {
        List<String> colorsForGame = new ArrayList<>(PLAYER_COLOR_SET);
        return users.stream()
                .map(user -> {
                    String playerColor = getPlayerColor(colorsForGame);
                    Player player = Player.builder()
                            .user(user)
                            .turnNumber(PLAYER_COLOR_SET.indexOf(playerColor))
                            .color(playerColor)
                            .build();
                    colorsForGame.remove(playerColor);
                    return player;
                })
                .collect(Collectors.toList());
    }

    public static PlayerDto mapToPlayerDto(Player player) {
        return PlayerDto.builder()
                .position(player.getPosition())
                .active(player.isActive())
                .inJail(player.isInJail())
                .user(UserMapper.mapToUserDto(player.getUser()))
                .balance(player.getBalance())
                .color(player.getColor())
                .build();
    }

    private static String getPlayerColor(List<String> colors) {
        return colors.get((int) (Math.random() * colors.size()));
    }
}