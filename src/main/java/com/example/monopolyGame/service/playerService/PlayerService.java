package com.example.monopolyGame.service.playerService;

import com.example.monopolyGame.entity.Player;
import com.example.monopolyGame.entity.User;

import java.util.List;

public interface PlayerService {

    List<Player> getPlayersForGame(List<User> users);
}