package com.example.monopolyGame.service.playerService;

import com.example.monopolyGame.entity.Player;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.messageManager.InfoMessageManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PlayerServiceImpl implements PlayerService {

    @Override
    public List<Player> getPlayersForGame(List<User> users) {
        List<Player> players = PlayerMapper.mapToNewPlayer(users);
        log.info(InfoMessageManager.CREATE_PLAYERS);
        return players;
    }
}