package com.example.monopolyGame.service.chatMessageService;

import com.example.monopolyGame.dto.ChatMessageDto;
import com.example.monopolyGame.entity.Chat;
import com.example.monopolyGame.entity.ChatMessage;
import com.example.monopolyGame.entity.MessageType;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.service.userService.UserMapper;

public class ChatMessageMapper {

    public static ChatMessage mapToChatMessage(ChatMessageDto chatMessage, Chat chat) {
        return ChatMessage.builder()
                .text(chatMessage.getText())
                .chat(chat)
                .type(MessageType.valueOf(chatMessage.getType()))
                .build();
    }

    public static ChatMessage mapToChatMessageWithSender(ChatMessageDto chatMessage, Chat chat, User sender) {
        return ChatMessage.builder()
                .text(chatMessage.getText())
                .chat(chat)
                .sender(sender)
                .type(MessageType.valueOf(chatMessage.getType()))
                .build();
    }

    public static ChatMessageDto mapToChatMessageDto(ChatMessage chatMessage) {
        ChatMessageDto chatMessageDto = ChatMessageDto.builder()
                .chatId(chatMessage.getChat().getId())
                .text(chatMessage.getText())
                .type(String.valueOf(chatMessage.getType()))
                .build();
        if (chatMessage.getSender() != null) {
            chatMessageDto.setSender(UserMapper.mapToUserDto(chatMessage.getSender()));
        }
        return chatMessageDto;
    }

    public static ChatMessageDto createChatMessageDto(long chatId, User user, String text, MessageType type) {
        return ChatMessageDto.builder()
                .chatId(chatId)
                .sender(UserMapper.mapToUserDto(user))
                .text(text)
                .type(String.valueOf(type))
                .build();
    }
}