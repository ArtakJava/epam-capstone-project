package com.example.monopolyGame.service.chatMessageService;

import com.example.monopolyGame.dto.ChatMessageDto;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface ChatMessageService {

    ChatMessageDto addMessage(ChatMessageDto chatMessage, Authentication authentication);

    List<ChatMessageDto> getMessagesByChat(long chatId);

    ChatMessageDto playerLeftTheGame(ChatMessageDto chatMessage);
}