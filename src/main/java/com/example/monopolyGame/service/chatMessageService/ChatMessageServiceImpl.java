package com.example.monopolyGame.service.chatMessageService;

import com.example.monopolyGame.dto.ChatMessageDto;
import com.example.monopolyGame.entity.Chat;
import com.example.monopolyGame.entity.ChatMessage;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.messageManager.InfoMessageManager;
import com.example.monopolyGame.repository.ChatMessageRepository;
import com.example.monopolyGame.service.chatService.ChatService;
import com.example.monopolyGame.service.userService.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChatMessageServiceImpl implements ChatMessageService {
    private final UserService userService;
    private final ChatService chatService;
    private final ChatMessageRepository chatMessageRepository;

    @Transactional
    @Override
    public ChatMessageDto addMessage(ChatMessageDto chatMessage, Authentication authentication) {
        Chat chat = chatService.findById(chatMessage.getChatId());
        User sender = userService.findByName(authentication.getName());
        ChatMessage message = ChatMessageMapper.mapToChatMessageWithSender(chatMessage, chat, sender);
        ChatMessageDto chatMessageDto = ChatMessageMapper.mapToChatMessageDto(chatMessageRepository.save(message));
        log.info(String.format(InfoMessageManager.ADD_MESSAGE, chatMessageDto));
        return chatMessageDto;
    }

    @Override
    public List<ChatMessageDto> getMessagesByChat(long publicChatId) {
        List<ChatMessageDto> messages = chatMessageRepository.findAllByChatId(publicChatId).stream()
                .map(ChatMessageMapper::mapToChatMessageDto)
                .collect(Collectors.toList());
        log.info(String.format(InfoMessageManager.GET_MESSAGES_BY_CHAT, publicChatId));
        return messages;
    }

    @Override
    public ChatMessageDto playerLeftTheGame(ChatMessageDto chatMessage) {
        Chat chat = chatService.findById(chatMessage.getChatId());
        ChatMessage message = ChatMessageMapper.mapToChatMessage(chatMessage, chat);
        ChatMessageDto chatMessageDto = ChatMessageMapper.mapToChatMessageDto(chatMessageRepository.save(message));
        log.info(String.format(InfoMessageManager.ADD_MESSAGE, chatMessageDto));
        return chatMessageDto;
    }
}