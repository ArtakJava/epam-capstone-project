package com.example.monopolyGame.service.userService;

import com.example.monopolyGame.dto.UserCreateDto;
import com.example.monopolyGame.entity.User;
import com.example.monopolyGame.messageManager.ErrorMessageManager;
import com.example.monopolyGame.messageManager.InfoMessageManager;
import com.example.monopolyGame.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public void createUser(UserCreateDto userCreateDto) {
        User user = UserMapper.mapToUser(userCreateDto);
        userRepository.save(user);
        log.info(String.format(InfoMessageManager.CREATE_USER, user.getName()));
    }

    @Override
    public User findByName(String name) {
        return userRepository.findByName(name).orElseThrow(
                () -> new UsernameNotFoundException(String.format(ErrorMessageManager.USER_NAME_NOT_FOUND, name))
        );
    }

    @Override
    public void setActiveUsers(List<User> players) {
        List<User> users = userRepository.findAllById(players.stream().map(User::getId).collect(Collectors.toSet()));
        users.forEach(user -> user.setActive(true));
        userRepository.saveAll(users);
        log.info(InfoMessageManager.SET_ACTIVE_FOR_PLAYERS);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByName(username);
        return new org.springframework.security.core.userdetails.User(
                user.getName(),
                user.getPassword(),
                List.of(new SimpleGrantedAuthority("ROLE_USER"))
        );
    }
}