package com.example.monopolyGame.service.userService;

import com.example.monopolyGame.dto.UserCreateDto;
import com.example.monopolyGame.dto.UserDto;
import com.example.monopolyGame.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private static PasswordEncoder passwordEncoder;

    public UserMapper(PasswordEncoder passwordEncoder) {
        UserMapper.passwordEncoder = passwordEncoder;
    }

    public static User mapToUser(UserCreateDto userCreateDto) {
        return User.builder()
                .name(userCreateDto.getName())
                .password(passwordEncoder.encode(userCreateDto.getPassword()))
                .email(userCreateDto.getEmail())
                .build();
    }

    public static UserDto mapToUserDto(User user) {
        return UserDto.builder()
                .name(user.getName())
                .active(user.isActive())
                .build();
    }
}