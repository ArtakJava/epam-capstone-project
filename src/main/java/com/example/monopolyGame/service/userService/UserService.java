package com.example.monopolyGame.service.userService;

import com.example.monopolyGame.dto.UserCreateDto;
import com.example.monopolyGame.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void createUser(UserCreateDto userCreateDto);

    User findByName(String email);

    void setActiveUsers(List<User> players);
}