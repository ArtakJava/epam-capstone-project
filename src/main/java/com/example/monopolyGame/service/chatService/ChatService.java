package com.example.monopolyGame.service.chatService;

import com.example.monopolyGame.entity.Chat;

public interface ChatService {

    Chat createChat();

    Chat findById(long chatId);
}