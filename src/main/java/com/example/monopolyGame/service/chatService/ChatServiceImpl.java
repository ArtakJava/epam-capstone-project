package com.example.monopolyGame.service.chatService;

import com.example.monopolyGame.entity.Chat;
import com.example.monopolyGame.exception.ChatNotFoundException;
import com.example.monopolyGame.messageManager.ErrorMessageManager;
import com.example.monopolyGame.messageManager.InfoMessageManager;
import com.example.monopolyGame.repository.ChatRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChatServiceImpl implements ChatService {
    private final ChatRepository chatRepository;

    @Override
    public Chat createChat() {
        Chat chat = chatRepository.save(new Chat());
        log.info(String.format(InfoMessageManager.CREATE_CHAT, chat.getId()));
        return chat;
    }

    @Override
    public Chat findById(long id) {
        return chatRepository.findById(id).orElseThrow(
                () -> new ChatNotFoundException(String.format(ErrorMessageManager.CHAT_NOT_FOUND, id))
        );
    }
}