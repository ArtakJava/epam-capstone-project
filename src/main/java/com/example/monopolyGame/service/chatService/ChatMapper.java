package com.example.monopolyGame.service.chatService;

import com.example.monopolyGame.dto.ChatDto;
import com.example.monopolyGame.entity.Chat;
import com.example.monopolyGame.service.chatMessageService.ChatMessageMapper;

import java.util.stream.Collectors;

public class ChatMapper {

    public static ChatDto mapToChatDto(Chat chat) {
        return ChatDto.builder()
                .id(chat.getId())
                .messages(chat.getMessages().stream()
                        .map(ChatMessageMapper::mapToChatMessageDto)
                        .collect(Collectors.toList()))
                .build();
    }
}