var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;
var localChatId = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connectChat(externalChatId, externalStompClient) {
    chatId = externalChatId;
    stompClient = externalStompClient;
    onConnectedChat();
}

function onConnectedChat() {
    if (currentUser) {
        messageForm.classList.remove('hidden');
    }
    stompClient.subscribe('/chats/public/' + chatId, onMessageReceived);
    stompClient.subscribe('/chats/public/' + chatId + '/getMessages', onMessagesReceived);
    getMessages();
    stompClient.send('/app/chats/' + chatId + '/getMessages', {}, JSON.stringify());
    connectingElement.classList.add('hidden');
}

function getMessages() {
    setInterval(() => {
        console.log('getMessages');
        stompClient.send('/app/chats/' + chatId + '/getMessages', {}, JSON.stringify());
    }, 600000);
}

function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    if(messageContent && stompClient) {
        var chatMessage = {
            chatId: chatId,
            sender: currentUser,
            text: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send('/app/chats/' + chatId + '/sendMessage', {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}

function onMessagesReceived(payload) {
    var oldMessages = document.querySelectorAll('.chat-message');
    if (oldMessages) {
        oldMessages.forEach(oldMessage => {
                oldMessage.remove();
            }
        )
    }
    var messages = JSON.parse(payload.body);
    messages.forEach(message => {
            processMessage(message);
        }
    )
}

function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
    processMessage(message);
}

function processMessage(message) {
    var messageElement = document.createElement('li');
    if (message.type === 'EVENT') {
        messageElement.classList.add('event-message');
    } else if (message.type === 'LEFT') {
        messageElement.classList.add('left-message');
    } else {
        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender.name[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender.name);
        messageElement.appendChild(avatarElement);
        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender.name);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }
    messageElement.classList.add('chat-message');
    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.text);
    textElement.appendChild(messageText);
    messageElement.appendChild(textElement);
    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

messageForm.addEventListener('submit', sendMessage, true)