var board = document.querySelector('.board');
var fieldsContainer = document.querySelector('.fields');
var playersContainer = document.querySelector('.players');
var gameId = document.querySelector('.game-id');
var wrappers = document.querySelectorAll('.wrapper');
var rollWrapper = document.querySelector('.wrapper-roll');
var rollAnswerWrapper = document.querySelector('.wrapper-roll-answer');
var payWrapper = document.querySelector('.wrapper-pay');
var diceOne = document.querySelector('.dice-one');
var diceTwo = document.querySelector('.dice-two');
var rollDiceButton = document.querySelector('.roll');
var buyButton = document.querySelector('.buy');
var rejectButton = document.querySelector('.reject');
var payButton = document.querySelector('.pay');
var fieldMenu = document.getElementById('menu');


var endTimeForTurn = null;
var stompClient = null;
var currentPlayer = null;
var players = null;
var timer = null;
var chatId = null;
var currentFieldPosition = null;

let constTimer = null;

connect();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected);
}

function onConnected() {
    if (gameId && currentUser && stompClient) {
        chatId = game.chat.id;
        connectChat(chatId, stompClient);
        loadGame(game);
        startTimer(game);
        stompClient.subscribe('/games/public/' + gameId.textContent, gameReceived);
        selectFieldsButtonForGame();
        showContainer(game);
    }
}

function selectFieldsButtonForGame() {
    fields = document.querySelectorAll('.field');
    setClickFunctions();
}

function setClickFunctions() {
    fields.forEach(button => {
            button.addEventListener('click', showFieldInfo, true);
        }
    )
}

function showFieldInfo(event) {
    var currentField = event.currentTarget;
    var fieldId = currentField.getAttribute('position');
    var field = findFieldByPosition(fieldId);
    var fieldInfo;
    game.fields.forEach(field => {
            if (field.price != null && field.position == fieldId) {
                var fieldInfoDiv = document.createElement('div');
                currentFieldPosition = field.position;
                if (field.level != null && field.group.levelUpCost != 0) {
                    var levelUpInfo = document.createElement('div');
                    levelUpInfo.classList.add('field-menu-item');
                    var levelUpText = null;
                    if (currentUser.name == game.currentPlayer.user.name && game.currentPlayer.user.name == field.owner.user.name && field.upgradeable && !field.inBail) {
                        levelUpText = document.createElement('button');
                        levelUpText.classList.add('level-up-button');
                        levelUpText.addEventListener('click', levelUp, true);
                    } else {
                        levelUpText = document.createElement('span');
                    }
                    levelUpText.innerHTML = 'Level up';
                    levelUpInfo.appendChild(levelUpText);

                    var fieldLevelUp = document.createElement('span');
                    fieldLevelUp.innerHTML = field.group.levelUpCost;
                    levelUpInfo.appendChild(fieldLevelUp);

                    fieldInfoDiv.appendChild(levelUpInfo);

                    var baseRentInfo = document.createElement('div');
                    baseRentInfo.classList.add('field-menu-item');

                    var baseRentText = document.createElement('span');
                    baseRentText.innerHTML = 'Base Rent';
                    baseRentInfo.appendChild(baseRentText);

                    var fieldBaseRent = document.createElement('span');
                    fieldBaseRent.innerHTML = field.baseRent;
                    baseRentInfo.appendChild(fieldBaseRent);

                    fieldInfoDiv.appendChild(baseRentInfo);

                    var oneLevelFieldInfo = document.createElement('div');
                    oneLevelFieldInfo.classList.add('field-menu-item');

                    var oneLevel = getOneLevel();
                    oneLevelFieldInfo.appendChild(oneLevel);

                    var oneLevelRent = document.createElement('span');
                    oneLevelRent.innerHTML = field.firstLevelRent;
                    oneLevelFieldInfo.appendChild(oneLevelRent);

                    fieldInfoDiv.appendChild(oneLevelFieldInfo);

                    var twoLevelFieldInfo = document.createElement('div');
                    twoLevelFieldInfo.classList.add('field-menu-item');

                    var twoLevel = getTwoLevel();
                    twoLevelFieldInfo.appendChild(twoLevel);

                    var twoLevelRent = document.createElement('span');
                    twoLevelRent.innerHTML = field.secondLevelRent;
                    twoLevelFieldInfo.appendChild(twoLevelRent);

                    fieldInfoDiv.appendChild(twoLevelFieldInfo);

                    var threeLevelFieldInfo = document.createElement('div');
                    threeLevelFieldInfo.classList.add('field-menu-item');

                    var threeLevel = getThreeLevel();
                    threeLevelFieldInfo.appendChild(threeLevel);

                    var threeLevelRent = document.createElement('span');
                    threeLevelRent.innerHTML = field.thirdLevelRent;
                    threeLevelFieldInfo.appendChild(threeLevelRent);

                    fieldInfoDiv.appendChild(threeLevelFieldInfo);
                } else if (field.group.levelUpCost == 0) {
                    if (field.firstLevelFactor != null) {
                        var oneFactorFieldInfo = document.createElement('div');
                        oneFactorFieldInfo.classList.add('field-menu-item');

                        var oneFactorFieldText = document.createElement('span');
                        oneFactorFieldText.innerHTML = 'One field';
                        oneFactorFieldInfo.appendChild(oneFactorFieldText);

                        var oneFactorField = document.createElement('span');
                        oneFactorField.classList.add('factor-value');
                        var oneFactorImg = getFactorImg();
                        oneFactorField.appendChild(oneFactorImg);
                        var oneFactorFieldValue = document.createElement('span');
                        oneFactorFieldValue.innerHTML = ' X ' + field.firstLevelFactor;
                        oneFactorField.appendChild(oneFactorFieldValue);
                        oneFactorFieldInfo.appendChild(oneFactorField);

                        fieldInfoDiv.appendChild(oneFactorFieldInfo);

                        var twoFactorFieldInfo = document.createElement('div');
                        twoFactorFieldInfo.classList.add('field-menu-item');

                        var twoFactorFieldText = document.createElement('span');
                        twoFactorFieldText.innerHTML = 'Two field';
                        twoFactorFieldInfo.appendChild(twoFactorFieldText);

                        var twoFactorField = document.createElement('span');
                        twoFactorField.classList.add('factor-value');
                        var oneFactorImg = getFactorImg();
                        twoFactorField.appendChild(oneFactorImg);
                        var twoFactorFieldValue = document.createElement('span');
                        twoFactorFieldValue.innerHTML = ' X ' + field.secondLevelFactor;
                        twoFactorField.appendChild(twoFactorFieldValue);
                        twoFactorFieldInfo.appendChild(twoFactorField);

                        fieldInfoDiv.appendChild(twoFactorFieldInfo);
                    } else {
                        var oneFieldInfo = document.createElement('div');
                        oneFieldInfo.classList.add('field-menu-item');

                        var oneFieldText = document.createElement('span');
                        oneFieldText.innerHTML = 'One field';
                        oneFieldInfo.appendChild(oneFieldText);

                        var oneFieldRent = document.createElement('span');
                        oneFieldRent.innerHTML = field.baseRent;
                        oneFieldInfo.appendChild(oneFieldRent);

                        fieldInfoDiv.appendChild(oneFieldInfo);

                        var twoFieldInfo = document.createElement('div');
                        twoFieldInfo.classList.add('field-menu-item');

                        var twoFieldText = document.createElement('span');
                        twoFieldText.innerHTML = 'Two field';
                        twoFieldInfo.appendChild(twoFieldText);

                        var twoFieldRent = document.createElement('span');
                        twoFieldRent.innerHTML = field.firstLevelRent;
                        twoFieldInfo.appendChild(twoFieldRent);

                        fieldInfoDiv.appendChild(twoFieldInfo);

                        var threeFieldInfo = document.createElement('div');
                        threeFieldInfo.classList.add('field-menu-item');

                        var threeFieldText = document.createElement('span');
                        threeFieldText.innerHTML = 'Three field';
                        threeFieldInfo.appendChild(threeFieldText);

                        var threeFieldRent = document.createElement('span');
                        threeFieldRent.innerHTML = field.secondLevelRent;
                        threeFieldInfo.appendChild(threeFieldRent);

                        fieldInfoDiv.appendChild(threeFieldInfo);

                        var fourFieldInfo = document.createElement('div');
                        fourFieldInfo.classList.add('field-menu-item');

                        var fourFieldText = document.createElement('span');
                        fourFieldText.innerHTML = 'Two field';
                        fourFieldInfo.appendChild(fourFieldText);

                        var fourFieldRent = document.createElement('span');
                        fourFieldRent.innerHTML = field.thirdLevelRent;
                        fourFieldInfo.appendChild(fourFieldRent);

                        fieldInfoDiv.appendChild(fourFieldInfo);
                    }
                }
                fieldMenu.innerHTML = '';
                var vh = window.innerHeight / 100;
                var fieldInfoHeader = document.createElement('span');
                var fieldInfoName = document.createElement('span');
                fieldInfoName.innerHTML = field.name;
                fieldInfoHeader.appendChild(fieldInfoName);
                var fieldInfoGroup = document.createElement('span');
                fieldInfoGroup.innerHTML = field.group.name;
                fieldInfoGroup.classList.add('field-group-text');
                fieldInfoHeader.appendChild(fieldInfoGroup);
                fieldInfoHeader.classList.add('field-info-text');
                fieldInfoHeader.style.backgroundColor = field.group.priceColor;
                fieldMenu.appendChild(fieldInfoHeader);
                if (currentField.classList.contains("top-line")) {
                    var fieldLeft = window.getComputedStyle(currentField).getPropertyValue("left");
                    var fieldTop = window.getComputedStyle(currentField).getPropertyValue("top");
                    var x = (parseFloat(fieldLeft) / vh) - 6;
                    var y = (parseFloat(fieldTop) / vh) + 12;
                    fieldMenu.style.right = '';
                    fieldMenu.style.bottom = '';
                    fieldMenu.style.left = x + 'vh';
                    fieldMenu.style.top = y + 'vh';
                }
                if (currentField.classList.contains("right-line")) {
                    var fieldRight = window.getComputedStyle(currentField).getPropertyValue("right");
                    var fieldTop = window.getComputedStyle(currentField).getPropertyValue("top");
                    var x = (parseFloat(fieldRight) / vh) + 10.2;
                    var y = (parseFloat(fieldTop) / vh) + 2;
                    if (y > 70) {
                        y = y - 25.5;
                    } else if (y > 60) {
                        y = y - 17.5;
                    } else if (y > 50) {
                        y = y - 1.5;
                    }
                    fieldMenu.style.left = '';
                    fieldMenu.style.bottom = '';
                    fieldMenu.style.right = x + 'vh';
                    fieldMenu.style.top = y + 'vh';
                }
                if (currentField.classList.contains("bottom-line")) {
                    var fieldLeft = window.getComputedStyle(currentField).getPropertyValue("left");
                    var fieldBottom = window.getComputedStyle(currentField).getPropertyValue("bottom");
                    var x = (parseFloat(fieldLeft) / vh) - 6;
                    var y = (parseFloat(fieldBottom) / vh) + 12.3;
                    fieldMenu.style.right = '';
                    fieldMenu.style.top = '';
                    fieldMenu.style.left = x + 'vh';
                    fieldMenu.style.bottom = y + 'vh';
                }
                if (currentField.classList.contains("left-line")) {
                    var fieldLeft = window.getComputedStyle(currentField).getPropertyValue("left");
                    var fieldTop = window.getComputedStyle(currentField).getPropertyValue("top");
                    var x = (parseFloat(fieldLeft) / vh) + 10;
                    var y = (parseFloat(fieldTop) / vh) + 2;
                    if (y > 70) {
                        y = y - 23.6;
                    } else if (y > 60) {
                        y = y - 15.6;
                    }
                    fieldMenu.style.right = '';
                    fieldMenu.style.bottom = '';
                    fieldMenu.style.left = x + 'vh';
                    fieldMenu.style.top = y + 'vh';
                }
                var priceInfo = document.createElement('div');
                priceInfo.classList.add('field-menu-item');

                var priceText = document.createElement('span');
                priceText.innerHTML = 'Price';
                priceInfo.appendChild(priceText);

                var fieldPrice = document.createElement('span');
                fieldPrice.innerHTML = field.price;
                priceInfo.appendChild(fieldPrice);

                fieldInfoDiv.appendChild(priceInfo);

                var bailInfo = document.createElement('div');
                bailInfo.classList.add('field-menu-item');

                var bailText = null;
                if (currentUser.name == game.currentPlayer.user.name && game.currentPlayer.user.name == field.owner.user.name && !field.inBail) {
                    bailText = document.createElement('button');
                    bailText.classList.add('bail-button');
                    bailText.addEventListener('click', setInBail, true);
                } else {
                    bailText = document.createElement('span');
                }
                bailText.innerHTML = 'Bail';
                bailInfo.appendChild(bailText);

                var fieldBail = document.createElement('span');
                fieldBail.innerHTML = field.bail;
                bailInfo.appendChild(fieldBail);

                fieldInfoDiv.appendChild(bailInfo);

                var redemptionInfo = document.createElement('div');
                redemptionInfo.classList.add('field-menu-item');

                var redemptionText = null;
                if (currentUser.name == game.currentPlayer.user.name && game.currentPlayer.user.name == field.owner.user.name && field.inBail) {
                    redemptionText = document.createElement('button');
                    redemptionText.classList.add('redemption-button');
                    redemptionText.addEventListener('click', setInRedemption, true);
                } else {
                    redemptionText = document.createElement('span');
                }
                redemptionText.innerHTML = 'Redemption';
                redemptionInfo.appendChild(redemptionText);

                var fieldRedemption = document.createElement('span');
                fieldRedemption.innerHTML = field.redemption;
                redemptionInfo.appendChild(fieldRedemption);

                fieldInfoDiv.appendChild(redemptionInfo);
                fieldMenu.appendChild(fieldInfoDiv);
            }
        }
    )
    fieldMenu.classList.remove('hidden');
}

function getOneLevel() {
    var oneLevel = document.createElement('div');
    oneLevel.classList.add('level');
    var oneLevelImg = getLevelStar();
    oneLevel.appendChild(oneLevelImg);
    return oneLevel;
}

function getTwoLevel() {
    var twoLevel = document.createElement('div');
    twoLevel.classList.add('level');
    var oneLevelImg = getLevelStar();
    twoLevel.appendChild(oneLevelImg);
    var twoLevelImg = getLevelStar();
    twoLevel.appendChild(twoLevelImg);
    return twoLevel;
}

function getThreeLevel() {
    var threeLevel = document.createElement('div');
    threeLevel.classList.add('level');
    var oneLevelImg = getLevelStar();
    threeLevel.appendChild(oneLevelImg);
    var twoLevelImg = getLevelStar();
    threeLevel.appendChild(twoLevelImg);
    var threeLevelImg = getLevelStar();
    threeLevel.appendChild(threeLevelImg);
    return threeLevel;
}

function getLevelStar() {
    var levelImg = document.createElement('img');
    levelImg.classList.add('level-img');
    levelImg.src = '/images/level.png';
    return levelImg;
}

function getFactorImg() {
    var factorImg = document.createElement('img');
    factorImg.classList.add('factor-img');
    factorImg.src = '/images/default-image-dice.png';
    return factorImg;
}

function levelUp(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/levelUp/" + currentFieldPosition, {}, JSON.stringify());
    }
}

function setInBail(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/setInBail/" + currentFieldPosition, {}, JSON.stringify());
    }
}

function setInRedemption(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/setInRedemption/" + currentFieldPosition, {}, JSON.stringify());
    }
}

function loadGame(updatedGame) {
    loadFields(updatedGame);
    loadPlayers(updatedGame);
    endTimeForTurn = document.querySelector('.players').getAttribute('end-time-for-turn');
    currentPlayer = document.querySelector('.current-player');
    players = document.querySelectorAll('.player');
}

function loadFields(game) {
    var fields = game.fields;
    fields.forEach(field => {
            var fieldContainer = document.createElement('button');
            fieldContainer.classList.add('field');
            fieldContainer.setAttribute("position", field.position);
            var fieldPlace = document.createElement('div');
            fieldPlace.classList.add('place');
            if (field.position == 11) {
                var deployPlace = document.createElement('div');
                deployPlace.classList.add('deploy-place');
                fieldPlace.appendChild(deployPlace);
                var simpleDayPlace = document.createElement('div');
                simpleDayPlace.classList.add('simple-place');
                fieldPlace.appendChild(simpleDayPlace);
            }
            fieldContainer.appendChild(fieldPlace);
            if (field.position > 1 && field.position < 11) {
                fieldContainer.classList.add('top-line');
            }
            if (field.position > 11 && field.position < 21) {
                fieldContainer.classList.add('right-line');
            }
            if (field.position > 21 && field.position < 31) {
                fieldContainer.classList.add('bottom-line');
            }
            if (field.position > 31 && field.position <= 40) {
                fieldContainer.classList.add('left-line');
            }
            if (field.position == 1 || field.position == 11 ||
            field.position == 21 || field.position == 31) {
                fieldContainer.classList.add('corner');
            }
            if (field.group) {
                fieldContainer.setAttribute("group", field.group.name);
            }
            var fieldInfo = getFieldInfo(field);
            fieldContainer.appendChild(fieldInfo);
            fieldsContainer.appendChild(fieldContainer);
        }
    )
}

function getFieldInfo(field) {
    var img = document.createElement('img');
    img.src = field.imgSrc;
    img.classList.add('field-img');
    var fieldInfo = document.createElement('div');
    fieldInfo.classList.add('field-info');
    fieldInfo.appendChild(img);
    if (field.owner) {
        if (!field.inBail) {
            fieldInfo.style.backgroundColor = field.owner.color;
        } else {
            fieldInfo.style.backgroundColor = '#9aabbf';
        }
        if (field.currentRent) {
            var rent = document.createElement("div");
            rent.classList.add('rent');
            if (!field.inBail) {
                rent.innerHTML = field.currentRent;
                rent.style.backgroundColor = field.group.rentColor;
            } else {
                rent.innerHTML = '0';
                rent.style.backgroundColor = '#353535';
            }
            fieldInfo.appendChild(rent);
        } else if (field.factor) {
            var factor = document.createElement("div");
            factor.classList.add('factor');
            if (!field.inBail) {
                factor.innerHTML = field.factor + 'X';
                factor.style.backgroundColor = field.group.rentColor;
            } else {
                factor.innerHTML = '0';
                factor.style.backgroundColor = '#353535';
            }
            fieldInfo.appendChild(factor);
        }
        var currentLevel = document.createElement('div');
        if (field.level == 1) {
            currentLevel = getOneLevel();
        } else if (field.level == 2) {
            currentLevel = getTwoLevel();
        } else if (field.level == 3) {
            currentLevel = getThreeLevel();
        }
        currentLevel.classList.add('current-level');
        fieldInfo.appendChild(currentLevel);
    } else if (field.price) {
        var price = document.createElement("div");
        price.classList.add('price');
        price.innerHTML = field.price;
        price.style.backgroundColor = field.group.priceColor;
        fieldInfo.appendChild(price);
    }
    return fieldInfo;
}

function loadPlayers(game) {
    var playersInfo = game.players;
    playersInfo.forEach(player => {
            var playerButton = document.createElement('button');
            playerButton.classList.add('player');
            playerButton.setAttribute("name", player.user.name);
            var playerInfo = document.createElement("div");
            playerInfo.classList.add('player-info');
            if (game.currentPlayer.user.name == player.user.name) {
                playerButton.classList.add('current-player');
                playerButton.style.backgroundColor = player.color;
                var timerForPlayer = document.createElement("div");
                timerForPlayer.setAttribute("id", 'timer');
                playerInfo.appendChild(timerForPlayer);
                timer = timerForPlayer;
            }
            if (currentUser.name == player.user.name) {
                var leaveImg = document.createElement("img");
                leaveImg.classList.add('leave-img');
                leaveImg.classList.add('hidden');
                leaveImg.src = "/images/leave.png";
                playerInfo.appendChild(leaveImg);
                leaveImg.addEventListener('click', leaveGame);
                playerInfo.addEventListener('mouseover', function() {
                    leaveImg.classList.remove('hidden');
                });
                playerInfo.addEventListener('mouseout', function() {
                    leaveImg.classList.add('hidden');
                });
            }
            var portfolio = document.createElement("div");
            portfolio.classList.add('portfolio');
            var avatar = document.createElement("div");
            avatar.classList.add('avatar');
            var img = document.createElement('img');
            img.src = "/images/player.png";
            img.classList.add('player-img');
            img.style.backgroundColor = player.color;
            avatar.appendChild(img);
            var playerName = document.createElement("span");
            playerName.innerHTML = player.user.name;
            var playerBalance = document.createElement("span");
            playerBalance.classList.add('balance');
            playerBalance.innerHTML = player.balance;
            portfolio.appendChild(avatar);
            portfolio.appendChild(playerName);
            portfolio.appendChild(playerBalance);
            playerInfo.appendChild(portfolio);
            playerButton.appendChild(playerInfo);
            playersContainer.appendChild(playerButton);
            setPlayerPosition(player);
            notActive(player);
        }
    )
}

function setPlayerPosition(player) {
    var playerFigure = document.querySelector('.player-figure.' + player.user.name);
    if (!playerFigure) {
        playerFigure = document.createElement("div");
        playerFigure.style.backgroundColor = player.color;
        playerFigure.classList.add('player-figure');
        playerFigure.classList.add(player.user.name);
    }
    var playerPosition = player.position;
    var field = findFieldByPosition(playerPosition);
    if (playerPosition != 11) {
        field.querySelector('.place').appendChild(playerFigure);
    } else {
        console.log(player.inJail);
        if (player.inJail) {
            field.querySelector('.place').querySelector('.deploy-place').appendChild(playerFigure);
        } else {
            field.querySelector('.place').querySelector('.simple-place').appendChild(playerFigure);
        }
    }
}

function startTimer(game) {
    endTimeForTurn = game.endTimeForTurn;
    if (game.state != 'FINISHED') {
        if (constTimer !== null) {
            clearInterval(constTimer);
        }
        var timerTime = Math.round((new Date(endTimeForTurn) - new Date()) / 1000) - 1;
        constTimer = setInterval(() => {
            timer.innerHTML = timerTime;
            timer.classList.remove('hidden');
            if (timerTime <= 0) {
                clearInterval(constTimer)
            } else {
                timerTime--;
            }
        }, 1000);
    } else {
        clearInterval(constTimer);
    }
}

function gameReceived(payload) {
    var updatedGame = JSON.parse(payload.body);
    game = updatedGame;
    if (!updatedGame.nextTurn) {
        if (updatedGame.diceResult) {
            processDiceResult(updatedGame.diceResult);
        }
        updatedGame.players.forEach(player => {
                setPlayerPosition(player);
            }
        )
        var updatedFields = updatedGame.updatedFields;
        if (updatedFields != null) {
            updatedFields.forEach(field => {
                    updateField(field);
                }
            )
        }
        updatePlayersBalance(updatedGame);
        players = document.querySelectorAll('.player');
    }
    if (game.state == 'ROLL' || game.state == 'FINISHED') {
        nextTurn(updatedGame);
    }
    if (game.state == 'FINISHED') {
        stompClient.disconnect();
    }
    showContainer(game);
}

function updateField(updatedField) {
    var oldField = findFieldByPosition(updatedField.position);
    var oldFieldInfo = oldField.querySelector('.field-info');
    oldField.removeChild(oldFieldInfo);
    var updatedFieldInfo = getFieldInfo(updatedField);
    oldField.appendChild(updatedFieldInfo);
}

function updatePlayersBalance(updatedGame) {
    var updatedPlayers = updatedGame.players;
    updatedPlayers.forEach(player => {
            var oldPlayer = findPlayer(player.user.name);
            var balanceDiv = oldPlayer.querySelector('.player-info').querySelector('.portfolio').querySelector('.balance');
            balanceDiv.innerHTML = player.balance;
        }
    )
}

function findFieldByPosition(position) {
    var field = document.querySelector('[position="' + position +'"]');
    return field;
}

function processDiceResult(diceResult) {
    var firstNumber = diceResult.firstNumber;
    var secondNumber = diceResult.secondNumber;
    var firstImage = getDiceImageForNumber(firstNumber);
    var secondImage = getDiceImageForNumber(secondNumber);
    diceOne.src = firstImage;
    diceTwo.src = secondImage;
    diceOne.classList.remove('hidden');
    diceTwo.classList.remove('hidden');
    setTimeout(hiddenDice, 3000);
}

function hiddenDice() {
    diceOne.classList.add('hidden');
    diceTwo.classList.add('hidden');
}

function getDiceImageForNumber(number) {
    switch (number) {
      case 1:
        return "/images/number-one.png";
      case 2:
        return "/images/number-two.png";
      case 3:
        return "/images/number-three.png";
      case 4:
        return "/images/number-four.png";
      case 5:
        return "/images/number-five.png";
      case 6:
        return "/images/number-six.png";
      default:
        return null;
    }
}

function nextTurn(game) {
    if (currentPlayer) {
        currentPlayer.classList.remove('current-player');
        currentPlayer.style.backgroundColor = '#222222';
    }
    var newCurrentPlayer = game.currentPlayer;
    var newPlayers = game.players;
    newPlayers.forEach(player => {
            notActive(player);
        }
    )
    players.forEach(player => {
            if (player.getAttribute('name') == newCurrentPlayer.user.name) {
                player.classList.add('current-player');
                player.style.backgroundColor = newCurrentPlayer.color;
                var playerInfo = player.querySelector('.player-info');
                if (game.state == 'FINISHED') {
                    playerInfo.style.backgroundColor = '#d0d50a';
                }
                timer.classList.add('hidden');
                playerInfo.appendChild(timer);
                currentPlayer = player;
                startTimer(game);
            }
        }
    )
}

function notActive(player) {
    if (!player.active) {
        var playerDiv = findPlayer(player.user.name);
        if (playerDiv) {
            playerDiv.classList.add('not-active')
            var img = playerDiv.querySelector('.player-info').querySelector('.portfolio').querySelector('.avatar').querySelector('img');
            img.style.backgroundColor = playerDiv.style.backgroundColor;
        }
    }
}

function findPlayer(name) {
    var result = null;
    players = document.querySelectorAll('.player');
    players.forEach(player => {
            if (player.getAttribute('name') == name) {
                result =  player;
            }
        }
    )
    return result;
}

function rollDice(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/rollDice", {}, JSON.stringify());
    }
    hiddenContainers()
}

function buyField(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/buyField", {}, JSON.stringify());
    }
    hiddenContainers()
}

function rejectField(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/rejectField", {}, JSON.stringify());
    }
    hiddenContainers();
}

function pay(event) {
    if(currentUser && stompClient) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/pay", {}, JSON.stringify());
    }
    hiddenContainers();
}

function showRollContainer(game) {
    hiddenContainers();
    if (currentUser.name == currentPlayer.getAttribute('name')) {
        rollWrapper.classList.remove('hidden');
    } else {
        hiddenContainers();
    }
}

function showContainer(game) {
    if (game.state == 'ROLL') {
        showRollContainer(game);
    }
    if (game.state == 'BUY_ANSWER') {
        showBuyContainer(game);
    }
    if (game.state == 'PAY') {
        showPayContainer(game);
    }
}

function showBuyContainer(game) {
    hiddenContainers();
    if (currentUser.name == currentPlayer.getAttribute('name')) {
        rollAnswerWrapper.classList.remove('hidden');
    } else {
        hiddenContainers();
    }
}

function showPayContainer(game) {
    hiddenContainers();
    if (currentUser.name == currentPlayer.getAttribute('name')) {
        payWrapper.classList.remove('hidden');
    } else {
        hiddenContainers();
    }
}

function hiddenContainers() {
    wrappers.forEach(wrapper => {
            wrapper.classList.add('hidden');
        }
    )
}

function hiddenFieldInfo(event) {
    fieldMenu.classList.add('hidden');
}

function leaveGame(event) {
    if (currentUser) {
        stompClient.send("/app/games/public/" + gameId.textContent + "/leave", {}, JSON.stringify());
    } else {
        window.location.replace("/login");
    }
}

rollDiceButton.addEventListener('click', rollDice, true)
buyButton.addEventListener('click', buyField, true)
rejectButton.addEventListener('click', rejectField, true)
payButton.addEventListener('click', pay, true)
document.addEventListener('click', hiddenFieldInfo, true)