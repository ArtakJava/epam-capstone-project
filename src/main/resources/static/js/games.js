var createButton = document.querySelector('.create-game-button');
var submitCreateButton = document.querySelector('.submit-create-button');
var cancelCreateButton = document.querySelector('.cancel-create-button');
var searchButton = document.querySelector('.search-submit-button');
var defaultForm = document.querySelector('.default-form');
var createForm = document.querySelector('.create-form');
var numberTwoButton = document.querySelector('.number-2');
var numberThreeButton = document.querySelector('.number-3');
var numberFourButton = document.querySelector('.number-4');
var numberFiveButton = document.querySelector('.number-5');
var gameListArea = document.querySelector('.gameListArea');
var playerPlace = document.querySelector('.player-place');
var numberOfPlayers = 3;

var lobbies = null;
var myLobby = null;
var emptyPlayerButtons = null;
var stompClient = null;
var lobbyId = null;
var myPlace = null;
var myLobbySubscription = null;
var subscribedLobbyId = null;
var reconnectButton = null;

connect();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected);
    selectButtonsForGame();
}

function selectButtonsForGame() {
    lobbies = document.querySelectorAll('.lobby');
    emptyPlayerButtons = document.querySelectorAll('.empty-player');
    setClickFunctions();
}

function setClickFunctions() {
    emptyPlayerButtons.forEach(button => {
            button.addEventListener('click', connectToLobby, true)
        }
    )
    lobbies.forEach(lobby => {
            lobby.addEventListener('click', setLobbyId, true)
        }
    )
}

function onConnected() {
    reconnectButton = document.querySelector('.reconnect-button');
    if (reconnectButton) {
        reconnectButton.addEventListener('click', reconnect, true);
    }
    stompClient.subscribe('/user/lobbies/public', onLobbyReceived);
    stompClient.subscribe('/lobbies/public', onLobbiesReceived);
    getLobbies();
}

function getLobbies() {
    setInterval(() => {
        console.log('getMessages');
        stompClient.send("/app/lobbies/getLobbies", {}, JSON.stringify());
    }, 600000);
}

function showCreateForm(event) {
    if (currentUser) {
        selectNumberThree();
        defaultForm.classList.add('hidden');
        createForm.classList.remove('hidden');
    } else {
        window.location.replace("/login");
    }
}

function showDefaultForm(event) {
    resetSelectColor();
    createForm.classList.add('hidden');
    defaultForm.classList.remove('hidden');
}

function selectNumberTwo(event) {
    numberTwoButton.style.background = '#0B60B0';
    numberThreeButton.style.background = '#222222';
    numberFourButton.style.background = '#222222';
    numberFiveButton.style.background = '#222222';
    numberOfPlayers = 2;
}

function selectNumberThree(event) {
    numberTwoButton.style.background = '#222222';
    numberThreeButton.style.background = '#0B60B0';
    numberFourButton.style.background = '#222222';
    numberFiveButton.style.background = '#222222';
    numberOfPlayers = 3;
}

function selectNumberFour(event) {
    numberTwoButton.style.background = '#222222';
    numberThreeButton.style.background = '#222222';
    numberFourButton.style.background = '#0B60B0';
    numberFiveButton.style.background = '#222222';
    numberOfPlayers = 4;
}

function selectNumberFive(event) {
    numberTwoButton.style.background = '#222222';
    numberThreeButton.style.background = '#222222';
    numberFourButton.style.background = '#222222';
    numberFiveButton.style.background = '#0B60B0';
    numberOfPlayers = 5;
}

function createLobby(event) {
    if(currentUser && stompClient && numberOfPlayers) {
        var lobby = {
            numberOfPlayers: numberOfPlayers
        };
        stompClient.send("/app/lobbies/createLobby", {}, JSON.stringify(lobby));
    }
    showDefaultForm();
}

function resetSelectColor() {
    numberTwoButton.style.background = '#222222';
    numberThreeButton.style.background = '#222222';
    numberFourButton.style.background = '#222222';
    numberFiveButton.style.background = '#222222';
}

function onLobbiesReceived(payload) {
    if (lobbies) {
        lobbies.forEach(lobby => {
                lobby.remove();
            }
        )
    }
    var newLobbies = JSON.parse(payload.body);
    newLobbies.forEach(lobby => {
            processLobby(lobby);
        }
    )
}

function onLobbyReceived(payload) {
    if (lobbies) {
        lobbies.forEach(lobby => {
                lobby.remove();
            }
        )
    }
    var newLobby = JSON.parse(payload.body);
    processLobby(newLobby);
}

function processLobby(lobby) {
    var lobbyElement = document.createElement('li');
    lobbyElement.classList.add('lobby');
    lobbyElement.setAttribute('id', lobby.id);
    var players = lobby.players;
    players.forEach(player => {
            var playerElement = document.createElement('button');
            playerElement.classList.add('player-place');
            if (currentUser) {
                if (currentUser.name == player.name) {
                    playerElement.classList.add('my-place');
                } else {
                    playerElement.classList.add('player');
                }
            } else {
                    playerElement.classList.add('player');
            }
            var playerNameElement = document.createElement('span');
            var playerName = document.createTextNode(player.name);
            playerNameElement.appendChild(playerName);
            playerElement.appendChild(playerNameElement);
            lobbyElement.appendChild(playerElement);
       }
    )
    var activePlayersNumber = lobbyElement.children.length;
    var numberOfAllPlayers = lobby.numberOfPlayers;
    var emptyPlaces = numberOfAllPlayers - activePlayersNumber;
    for (let i = 0; i < emptyPlaces; i++) {
        var playerPlace = document.createElement('button');
        playerPlace.classList.add('player-place');
        playerPlace.classList.add('empty-player');
        var playerPlaceTextElement = document.createElement('span');
        var playerPlaceText = document.createTextNode('CONNECT TO PLAY GAME');
        playerPlaceTextElement.appendChild(playerPlaceText);
        playerPlace.appendChild(playerPlaceTextElement);
        lobbyElement.appendChild(playerPlace);
    }
    gameListArea.appendChild(lobbyElement);
    gameListArea.scrollTop = gameListArea.scrollHeight;
    selectButtonsForGame();
    changeMyLobbyPlaceOnTop();
}

function setLobbyId(event) {
    lobbyId = event.currentTarget.getAttribute('id');
}

function connectToLobby(event) {
    if(currentUser && stompClient) {
        var requestToConnectGame = {
            lobbyId: lobbyId
        };
        subscribeToConnectedGame(lobbyId);
        stompClient.send("/app/lobbies/connectToLobby", {}, JSON.stringify(requestToConnectGame));
    } else {
        window.location.replace("/login");
    }
}

function subscribeToConnectedGame(lobbyId) {
    if (myLobbySubscription) {
        if (subscribedLobbyId != lobbyId) {
            myLobbySubscription.unsubscribe();
            myLobbySubscription = stompClient.subscribe('/lobbies/public/' + lobbyId, onFullLobbyReceived);
            subscribedLobbyId = lobbyId;
        }
    } else {
        myLobbySubscription = stompClient.subscribe('/lobbies/public/' + lobbyId, onFullLobbyReceived);
        subscribedLobbyId = lobbyId;
    }
}

function onFullLobbyReceived(payload) {
    var game = JSON.parse(payload.body);
    window.location.replace("/games/" + game.id);
}

function changeMyLobbyPlaceOnTop() {
    myPlace = document.querySelector('.my-place');
    if (myPlace) {
        myLobby = myPlace.parentNode;
        myLobby.classList.add('my-lobby');
        subscribeToConnectedGame(myLobby.getAttribute('id'));
        gameListArea.prepend(myLobby);
    }
}

function search(event) {
    if (currentUser) {
        var lobbyId = document.getElementById("lobby-id-input").value;
        document.getElementById("lobby-id-input").value = '';
        stompClient.send("/app/lobbies/getLobbies/" + lobbyId, {}, JSON.stringify());
    } else {
        window.location.replace("/login");
    }
}

function reconnect(event) {
    if (currentUser) {
        window.location.replace("/games/reconnect");
    } else {
        window.location.replace("/login");
    }
}

createButton.addEventListener('click', showCreateForm, true);
submitCreateButton.addEventListener('click', createLobby, true);
cancelCreateButton.addEventListener('click', showDefaultForm, true);
searchButton.addEventListener('click', search, true);
numberTwoButton.addEventListener('click', selectNumberTwo, true);
numberThreeButton.addEventListener('click', selectNumberThree, true);
numberFourButton.addEventListener('click', selectNumberFour, true);
numberFiveButton.addEventListener('click', selectNumberFive, true);